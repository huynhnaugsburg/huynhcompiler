.data
dollars:   .double   0.0, 0.0, 0.0, 0.0, 0.0
yen:   .word   0, 0, 0, 0, 0


.text
main:
li.d  $f4, 5.0 
ldc1     $f8,   dollars
li.d  $f8, 5.0 
li.d  $f10, 1.0 
add.d    $f8,   $f8,   $f10
li.d $f8, -1.0
sll $f8, $f8, 2
add.d $f8, $f8, $f8
lw $f6, 0($f8)
ldc1     $f6,   dollars

add.d    $f0,   $f4,   $f6
ldc1     $f2,   dollars
li.d  $f4, 5.0 
li.d  $f6, 1.0 
add.d    $f2,   $f4,   $f6
li.d $f2, -1
sll $f2, $f2, 2
add.d $f2, $f2, $f2
sw  $f0, 0($f2)

li  $v0,   10
syscall

