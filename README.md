HuynhCompiler
Version 9

This project will be about designing a mini pascal compiler that can read in a file that will be then be
used to translate that information into MIPS assembly code.
----------------------
Folders:
SDD - Contains the software design document infromation for this project.
	SDD.tex - The raw latex file.
	SDD.pdf - A readable PDF version of the SDD.
src - Conatins the functional parts of the compiler
	compiler - Contains the main for the project and is what is used in the command line.
		CompilerMain.java - The main for the compiler code.
		CompilerMianTest.java - This tests the the compiler main.
		testgoodcode.pas - This is the code used for testing the the main.
	scanner - Contains the java files that are used for the scanner and their test files.
		Token.java - Token class
		TokenScanner.java - The main scanner part of the scanner.
		TokenScanner.jflex - The raw jflex file that is used to generate the TokenScanner.java
		TokenType.java - Holds the list of enums for the different token types.
		TokenTest.java - JUnit test file
		TokenScannerTest.java - JUnit test file.
		TokenTypeTest.java - JUnit test file.
	parser - Contains the java files that are used for the scanner and their test files.
		Recognizer.java - The main recognizer part of the package.
		RecognizerTest.java - The test funnctions for the recognizer.
		testgoodcode.pas - Used for testing a functional pascal code.
		testbadcode.pas - Used for testing bad pascal code.
	symboltable - Contains the java files that are used for the symbol table and their test files.
		KindType.java - Has the kind type enum for the code.
		KindTypeTest.java - Has a test files for the kind type enum.
		SymbolTable.java - Symbole table for the mini pascal language.
		SymbolTableTest.java - Has tests methods for the symbol table class.
	syntaxtree - Contains all of the nodes for the parser.
		AssignmentStatementNode.java
		CompoundStatementNode.java
		DeclarationsNode.java
		ExpressionNode.java
		IfStatementNode.java
		OperationNode.java
		ProgramNode.java
		ReadStatmentNode.java
		ReturnStatementNode.java
		StatementNode.java
		SubProgramDeclarationsNode.java
		SubProgramNode.java
		SyntaxTreeNode.java
		ValueNode.java
		VariableNode.java
		WhileDoStatementNode.java
		WriteStatemeNode.java
	analyzer
		DataType.java - Holds the enums for DataType
		DataTypeTest.java - Tests the enums for DataType
		SemanticAnalyzer.java - Checks if assignments are correct and operations given data types
		SemanticAnalyzerTest.java - Tests semantic analyzer.
		
----------------------
To run:
	Path your way to the src folder of the project.
	Then run this command so that you can compile the project:
		javac -cp . compiler/CompilerMain.java
	Next to run the compiled code on your own pascal file use this command:
		java compiler/CompilerMain PATH/TO/YOUR/PASCAL/CODE
----------------------
Built With:
JFlex - Makes the scanner.
Java - Runs the actual compiler.

----------------------
Author(s):
Nghia Huynh

----------------------
License:
This project is license under nobody and is free to use for all.

----------------------
Acknowledgements:
Google - Telling me what to do.