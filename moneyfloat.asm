.data
dollars:   .double   0.0
yen:   .double   0.0
bitcoins:   .double   0.0
five:   .double   0.0
sevenpointtwo:   .double   0.0


.text
main:
li.d  $f0, 7.2 
sdc1     $f0,   sevenpointtwo

li.d  $f0, 10000.0 
sdc1     $f0,   dollars

li.d  $f0, 5.0 
sdc1     $f0,   five

ldc1     $f4,   dollars
li.d  $f6, 110.0 
mul.d    $f0,   $f4,   $f6
sdc1     $f0,   yen

ldc1     $f4,   dollars
li.d  $f6, 3900.0 
div.d    $f0,   $f4,   $f6
sdc1     $f0,   bitcoins

li.d  $f0, 20.0 
sdc1     $f0,   yen

If1:
addi   $t0,   $zero, 2
addi   $t1,   $zero, 5
blt   $t0,   $t1,   Then1

Else1:
If2:
ldc1     $f4,   yen
li.d  $f6, 30.0 
c.eq.d   $f4,   $f6
bc1t Then2

Else2:
li.d  $f0, 62.0 
sdc1     $f0,   yen

j	Endif2

Then2:
li.d  $f0, 53.0 
sdc1     $f0,   yen

Endif2:

j	Endif1

Then1:
If3:
ldc1     $f4,   yen
li.d  $f6, 20.0 
c.eq.d   $f4,   $f6
bc1t Then3

Else3:
li.d  $f0, 89.0 
sdc1     $f0,   yen

j	Endif3

Then3:
li.d  $f0, 42.0 
sdc1     $f0,   yen

Endif3:

Endif1:

ldc1     $f12,   yen
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

While4:
ldc1     $f4,   five
li.d  $f6, 0.0 
c.le.d   $f6,   $f4
bc1t Do4

j	EndWhileDo4

Do4:
ldc1     $f12,   five
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

ldc1     $f4,   five
li.d  $f6, 0.1 
sub.d    $f0,   $f4,   $f6
sdc1     $f0,   five

j	While4

EndWhileDo4:

li $v0, 7
syscall
sdc1 $f0, bitcoins

ldc1     $f12,   bitcoins
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

addi   $t0,   $zero, 1
addi   $t1,   $zero, 1
add    $s0,   $t0,   $t1
li $v0, 1
add $a0, $s0, $zero
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

addi   $t0,   $zero, 1
addi   $t1,   $zero, 1
add    $v0,   $t0,   $t1


li.d  $f4, 5.0 
li.d  $f6, 5.0 
add.d    $f0,   $f4,   $f6
sdc1     $f0,   yen

ldc1     $f4,   yen
li.d  $f6, 20.0 
mul.d    $f0,   $f4,   $f6
sdc1     $f0,   dollars

ldc1     $f12,   yen
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

ldc1     $f12,   dollars
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

ldc1     $f12,   bitcoins
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

ldc1     $f12,   sevenpointtwo
li $v0, 3
syscall

addi $a0, $0, 0xA
addi $v0, $0, 0xB
syscall

li  $v0,   10
syscall

