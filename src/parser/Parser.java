/**
 * Parser.java
 * Compilers 2
 * February 24, 2019
 * This code is used to read in a string or file that contains pascal code and checks to make sure it 
 * is in the correct format.
 * @author Nghia Huynh
 * @version 2
 */
package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import com.sun.javafx.css.Declaration;

import analyzer.*;
import scanner.*;
import symboltable.*;
import syntaxtree.*;

public class Parser {
    /**
     * This a private token object
     */
    private Token lookahead;
    
    /**
     * This a private scanner object.
     */
    private TokenScanner scanner;
    
    /**
     * This is a private symboletable object
     */
    private SymbolTable table = new SymbolTable();
    
    private HashMap<String,ArrayNode> arraytable = new HashMap<String,ArrayNode>();
     
    /**
     * This the constructor for the recognizer class
     * takes in file or string and passes it the scanner.
     * @param text Takes and input of either a string or file.
     */
    public Parser(String text)
    {
    	//This line checks to see if the string ends with the pascal file type
    	//If it is true then we are dealing with a file.
    if( text.endsWith(".pas"))
    {
    	//This block is used to try and read the contents of the given file.
    	scanner = null;
    	try
    	{
            FileInputStream stream = new java.io.FileInputStream(text);
            InputStreamReader reader = new java.io.InputStreamReader(stream);
            scanner = new TokenScanner(reader);
    	}
    	catch(FileNotFoundException e)
    	{
    		error("No File Was Found!");
    	}
    }
    //If the above check fails then it will try to run the recognizer on the given string.
    else
    {
    	scanner = new TokenScanner(text);
    }
    //Try to grab next token if not then code shuts down.
    try 
    {
    	lookahead = scanner.nextToken();
    }
    catch (IOException e)
    {
    	error("Could not scan next token!");
    }
    }
    
    /**
     * This is a matching function that checks if the lookahead token is what we are looking for.
     * @param expected
     */
    public void match(TokenType expected)
    {
    	//Check to see if the next token is the correct one and if yes then call next token.
    	if(this.lookahead.type == expected)
    	{
    		try
    		{
    			this.lookahead = scanner.nextToken();
    			//Checks for end of the file.
    			if(this.lookahead == null)
    			{
    				this.lookahead = new Token("End of file!", null);
    			}
    		}
    		catch (IOException e)
    		{
    			error("Could not scan next token!");
    		}
    	}
    	else
    	{
    		error("Expected token of type " + expected + " , found a token of type " + this.lookahead.type + " instead!");
    	}
    }
    
    /**
     * NOTE: DO NOT USE THIS IS NOT A REGULAR PART OF THE RECOGNIZER IT IS FOR TESTING PURPOSES ONLY!
     * This adds an ID into the internal symboltable table object.
	 * @param newname	name for a new symbol
	 * @param newkind	kind for a new symbol
	 * @param newtype	type for a new symbol
     */
    public void addID(String newname, KindType newkind, DataType newtype)
    {
    	table.addSymbol(newname, newkind, newtype);;
    }
    
    /** Takes the tostring function from the SymbolTable.java and allows the recognizer to call this
     * and return its own clone of that toString.
     */
    public String toString()
    {
		return table.toString();
    }
    
    public SymbolTable getTable()
    {
    	return table;
    }
    
    /*END OF FUNCTIONS AND BEGIN GRAMMAR*/
    
    /**
     * This is the program grammer and is what is first called to start the recognizer.
     */
    public ProgramNode program()
    {
    	match(TokenType.PROGRAM);
    	table.addSymbol(lookahead.lexeme, KindType.PROGRAM, null);
    	ProgramNode pn = new ProgramNode(lookahead.lexeme);
    	match(TokenType.ID);
    	match(TokenType.SEMICOLON);
    	DeclarationsNode dn = declarations();
    	pn.setVariables(dn);
    	SubProgramDeclarationsNode sdn = subprogramDeclarations();
    	pn.setFunctions(sdn);
    	CompoundStatementNode csn = compoundStatement();
    	pn.setMain(csn);
    	match(TokenType.PERIOD);
		return pn;
    }
    
    /**
     * Checks for a list of identifiers.
     */
    public ArrayList<VariableNode> identifierList()
    {
    	ArrayList<VariableNode> idlist = new ArrayList<VariableNode>();
    	//table.addSymbol(lookahead.lexeme, KindType.VARIABLE, null);
    	idlist.add(new VariableNode(lookahead.lexeme));
		match(TokenType.ID);
		while (lookahead.type == TokenType.COMMA)
		{
			match(TokenType.COMMA);
			//table.addSymbol(lookahead.lexeme, KindType.VARIABLE, null);
	    	idlist.add(new VariableNode(lookahead.lexeme));
			match(TokenType.ID);
		}
		return idlist;
    }
    
    /**
     * Checks for the declarations within the programs.
     */
    public DeclarationsNode declarations()
    {
    	DeclarationsNode dn = new DeclarationsNode();
    	while (lookahead.type == TokenType.VARIABLE)
    	{
    		String start = null;
    		String end = null;
    		DataType type = null;
    		match(TokenType.VARIABLE);
    		ArrayList<VariableNode> idlist = identifierList();
    		match(TokenType.COLON);
        	if(lookahead.type == TokenType.ARRAY)
        	{
        		match(TokenType.ARRAY);
        		match(TokenType.LEFT_BRACKET);
        		start = lookahead.lexeme;
        		match(TokenType.NUMBER);
        		match(TokenType.COLON);
        		end = lookahead.lexeme;
        		match(TokenType.NUMBER);
        		match(TokenType.RIGHT_BRACKET);
        		match(TokenType.OF);
        	}
    		type = type();
    		for(int i = 0; i < idlist.size(); i++)
    		{
    			table.addSymbol(idlist.get(i).getName(), KindType.VARIABLE, type);
    			if(start == null && end == null)
    			{
    				idlist.get(i).setDataType(type);
        			dn.addVariable(idlist.get(i));
    			}
    			else
    			{
    				ArrayNode arraynode = new ArrayNode(idlist.get(i).getName());
    				arraynode.setSize(start, end);
    				arraynode.setOffSet(start);
    				arraynode.setDataType(type);
    				arraytable.put(idlist.get(i).getName(),arraynode);
    				dn.addVariable(arraynode);
    			}
    		}
    		match(TokenType.SEMICOLON);
    	}
		return dn;
    }
    
    /**
     * Checks the type of a part  of the code.
     */
    public DataType type()
    {
    	DataType answer = standardType();
    	return answer;
    }
    
    /**
     * Checks of the string is a standard type. Either and int or a real.
     */
    public DataType standardType()
    {
    	DataType answer = null;
    	if(lookahead.type == TokenType.INTEGER)
    	{
    		match(TokenType.INTEGER);
    		answer = DataType.INTEGER;
    		
    	}
    	else if(lookahead.type == TokenType.REAL)
    	{
    		match(TokenType.REAL);
    		answer = DataType.REAL;
    	}
    	else
    	{
    		error("Standard term not found!");
    	}
    	return answer;
    }
    
    /**
     * checks if a subprogram is being called by seeing if the keyword has been chosen.
     */
    public SubProgramDeclarationsNode subprogramDeclarations()
    {
    	SubProgramDeclarationsNode sdn = new SubProgramDeclarationsNode();
    	while(lookahead.type == TokenType.FUNCTION || lookahead.type == TokenType.PROCEDURE)
    	{
    		SubProgramNode spn = new SubProgramNode();
    		sdn.addSubProgramDeclaration(spn);
    		match(TokenType.SEMICOLON);
    	}
		return sdn;
    }
    
    /**
     * Declares a subprogram structure.
     */
    public SubProgramNode subprogramDeclaration()
    {
    	SubProgramNode spn = new SubProgramNode();
    	subprogramHead();
    	declarations();
    	compoundStatement();
		return spn;
    }
    
    /**
     * Start of the subprogram structure.
     */
    public void subprogramHead()
    {
    	if(lookahead.type == TokenType.FUNCTION)
    	{
    		match(TokenType.FUNCTION);
        	table.addSymbol(lookahead.lexeme, KindType.FUNCTION, null);
    		match(TokenType.ID);
    		arguments();
    		match(TokenType.COLON);
    		standardType();
    		match(TokenType.SEMICOLON);
    	}
    	else if(lookahead.type == TokenType.PROCEDURE)
    	{
    		match(TokenType.PROCEDURE);
        	table.addSymbol(lookahead.lexeme, KindType.PROCEDURE, null);
    		match(TokenType.ID);
    		arguments();
    		match(TokenType.SEMICOLON);
    	}
    	else
    	{
    		error("Subprogram head term not found!");
    	}
    }
    
    /**
     * Checks if an argument is being added to the code
     */
    public void arguments()
    {
    	if(lookahead.type == TokenType.LEFT_PARENTHESIS)
    	{
    		match(TokenType.LEFT_PARENTHESIS);
    		parameterList();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * Adds parameters to the system and if it sees a semi colon it adds some more.
     */
    public void parameterList()
    {
    	identifierList();
    	match(TokenType.COLON);
    	type();
    	if(lookahead.type == TokenType.SEMICOLON)
    	{
    		match(TokenType.SEMICOLON);
    		parameterList();
    	}
    }
    
    /**
     * This is the beginning of a compound statement.
     */
    public CompoundStatementNode compoundStatement()
    {
    	CompoundStatementNode csn = new CompoundStatementNode();
    	match(TokenType.BEGIN);
    	ArrayList<StatementNode> optionallist = statementList();
    	for(int i = 0; i < optionallist.size(); i++)
    	{
    		csn.addStatement(optionallist.get(i));
    	}
    	match(TokenType.END);
		return csn;
    }
    
    /**
     * This builds a list of statements that gets called again if a statement ends in a semi colon.
     */
    public ArrayList<StatementNode> statementList()
    {
    	ArrayList<StatementNode> statementlist = new ArrayList<StatementNode>();
    	if(lookahead.type != TokenType.END)
    	{
    		statementlist.add(statement());
    	}
		//System.out.println(csn.indentedToString(0));
    	while(lookahead.type == TokenType.SEMICOLON)
    	{
    		//System.out.println(csn.indentedToString(0));
    		match(TokenType.SEMICOLON);
    		statementlist.add(statement());
    	}
    	return statementlist;
    }
    
    /**
     * This holds the structure for a statement.
     * @return 
     */
    public StatementNode statement()
    {
    	//Check if the look ahead is any of these types and if it is go down that tree
    	StatementNode answer = null;
    	if(lookahead.type == TokenType.ID)
    	{
    		if(table.isVariable(lookahead.lexeme))
    		{
    			AssignmentStatementNode asn = new AssignmentStatementNode();
        		VariableNode variable = variable();
        		match(TokenType.ASSIGNOP);
        		asn.setLvalue(variable);
        		asn.setExpression(expression());
        		answer = asn;
    		}
    		else if(table.isProcedure(lookahead.lexeme))
    		{
    			procedureStatement();
    		}
    		else
    		{
    			error("Id is not of the correct kind!");
    		}
    	}
    	else if(lookahead.type == TokenType.BEGIN)
    	{
    		CompoundStatementNode csn = compoundStatement();
    		answer = csn;
    	}
    	else if(lookahead.type == TokenType.IF)
    	{
    		IfStatementNode ifn = new IfStatementNode();
    		match(TokenType.IF);
    		ExpressionNode ifstatement = expression();
    		match(TokenType.THEN);
    		StatementNode thenstatement = statement();
    		match(TokenType.ELSE);
    		StatementNode elsestatement = statement();
    		ifn.setTest(ifstatement);
    		ifn.setThenStatement(thenstatement);
    		ifn.setElseStatement(elsestatement);
    		answer = ifn;
    	}
    	else if(lookahead.type == TokenType.WHILE)
    	{
    		WhileDoStatementNode wdsn = new WhileDoStatementNode();
    		match(TokenType.WHILE);
    		wdsn.setExpression(expression());
    		match(TokenType.DO);
    		wdsn.setStatement(statement());
    		answer = wdsn;
    	}
    	else if(lookahead.type == TokenType.READ)
    	{
    		match(TokenType.READ);
    		match(TokenType.LEFT_PARENTHESIS);
    		ReadStatementNode rsn = new ReadStatementNode(lookahead.lexeme);
    		match(TokenType.ID);
    		match(TokenType.RIGHT_PARENTHESIS);
    		answer = rsn;
    	}
    	else if(lookahead.type == TokenType.WRITE)
    	{
    		WriteStatementNode wsn = new WriteStatementNode();
    		match(TokenType.WRITE);
    		match(TokenType.LEFT_PARENTHESIS);
    		wsn.setExpression(expression());
    		match(TokenType.RIGHT_PARENTHESIS);
    		answer = wsn;
    	}
    	else if(lookahead.type == TokenType.RETURN)
    	{
    		ReturnStatementNode rsn = new ReturnStatementNode();
    		match(TokenType.RETURN);
    		rsn.setExpression(expression());
    		answer = rsn;
    	}
    	else
    	{
    		error("Statement error!");
    	}
		return answer;
    }

	/**
	 * Checks for a variable and adds an expression if a left bracket is found.
	 */
	public VariableNode variable()
    {
		VariableNode vn = new VariableNode(lookahead.lexeme);
    	match(TokenType.ID);
    	if(lookahead.type == TokenType.LEFT_BRACKET)
    	{
    		ArrayNode an = arraytable.get(vn.getName());
    		match(TokenType.LEFT_BRACKET);
    		an.setExpression(expression());
    		match(TokenType.RIGHT_BRACKET);
    		return an;
    	}
		return vn;
    }
    
    /**
     * Begins a procedure if an id is found and if its followed by a parenthesis then the 
     * code adds an expression.
     */
    public void procedureStatement()
    {
    	match(TokenType.ID);
    	if(lookahead.type == TokenType.LEFT_PARENTHESIS)
    	{
    		match(TokenType.LEFT_PARENTHESIS);
    		expressionList();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    }
    
    /**
     * Creates a list of expressions with a chain if the expression ends with a comma.
     * @return 
     */
    public ArrayList<ExpressionNode> expressionList()
    {
    	ArrayList<ExpressionNode> expressionlist = new ArrayList<ExpressionNode>();
    	expressionlist.add(expression());
    	while(lookahead.type == TokenType.COMMA)
    	{
    		match(TokenType.COMMA);
    		expressionlist.add(expression());
    	}
		return expressionlist;
    }
    
    /**
     * A simple expression that checks if there is relop.
     */
    public ExpressionNode expression()
    {
    	ExpressionNode left = simpleExpression();
    	if(isRelop(lookahead))
    	{
    		OperationNode on = relop();
    		ExpressionNode right = simpleExpression();
    		on.setLeft(left);
    		on.setRight(right);
    		return on;
    	}
    	return left;
    }
    
    /**
     * simple expression is used to see the look ahead of the code.
     */
    public ExpressionNode simpleExpression()
    {
    	ExpressionNode answer = null;
    	if(lookahead.type == TokenType.ID
    	||	lookahead.type == TokenType.NUMBER
    	||	lookahead.type == TokenType.LEFT_PARENTHESIS
    	||	lookahead.type == TokenType.NOT
    	)
    	{
    		ExpressionNode aTerm = term();
    		answer = simplePart(aTerm);
    	}
    	else if(lookahead.type == TokenType.PLUS
    	|| lookahead.type == TokenType.MINUS)
    	{
    		sign();
    		ExpressionNode aTerm = term();
    		answer = simplePart(aTerm);
    	}
    	else
    	{
    		error("Simple expression not found!");
    	}
		return answer;
    }
    
    /**
     * This checks for if there is an addop at the begining and used to add in a simple part.
     */
    public ExpressionNode simplePart(ExpressionNode possibleLeft)
    {
    	if(isAddop(lookahead))
    	{
    		OperationNode on = addop();
    		ExpressionNode right = term();
    		on.setLeft(possibleLeft);
    		on.setRight(right);
    		return simplePart(on);
    	}
    	else
    	{
    		//Lambda
    		return possibleLeft;
    	}
    }
    
    /**
     * This represents a term within the grammar.
     * @return 
     */
    public ExpressionNode term()
    {
    	ExpressionNode left = factor();
    	return termPart(left);
    }
    
    /**
     * This sets the term part grammar.
     */
    public ExpressionNode termPart(ExpressionNode left)
    {
    	if(isMulop(lookahead))
    	{
    		OperationNode on = mulop();
    		ExpressionNode right = factor();
    		on.setLeft(left);
    		on.setRight(right);
        	return termPart(on);
    	}
    	else
    	{
    		//Lambda
    		return left;
    	}
    }
    
    /**
     * Checks if the function is viable factor.
     */
    public ExpressionNode factor()
    {
    	ExpressionNode answer = null;
    	if(lookahead.type == TokenType.ID)
    	{
    		if(table.isVariable(lookahead.lexeme))
    		{
    			answer = new VariableNode(lookahead.lexeme);
    		}
    		else
    		{
    			error(lookahead.lexeme + ": This is not a variable!");
    		}
    		String temp = lookahead.lexeme;
    		match(TokenType.ID);
    		if(lookahead.type == TokenType.LEFT_BRACKET)
    		{
    			ArrayNode an = arraytable.get(temp);
    			match(TokenType.LEFT_BRACKET);
    			an.setExpression(expression());
    			match(TokenType.RIGHT_BRACKET);
    			answer = an;
    		}
    		else if(lookahead.type == TokenType.LEFT_PARENTHESIS)
    		{
    			match(TokenType.LEFT_PARENTHESIS);
    			expressionList();
    			match(TokenType.RIGHT_PARENTHESIS);
    		}
    	}
    	else if (lookahead.type == TokenType.NUMBER)
    	{
    		Token value = lookahead;
    		match(TokenType.NUMBER);
    		answer = new ValueNode(value.lexeme);
    	}
    	else if (lookahead.type == TokenType.LEFT_PARENTHESIS)
    	{
    		match(TokenType.LEFT_PARENTHESIS);
    		answer = expression();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    	else if (lookahead.type == TokenType.NOT)
    	{
    		match(TokenType.NOT);
    		factor();
    	}
    	else
    	{
    		error("Factor term not found!");
    	}
    	return answer;
    }
    
    /**
     * Checks the sign of the function and see if its a plus or minus.
     */
    public void sign()
    {
    	if(lookahead.type == TokenType.PLUS)
    	{
    		match(TokenType.PLUS);
    	}
    	else if(lookahead.type == TokenType.MINUS)
    	{
    		match(TokenType.MINUS);
    	}
    	else
    	{
    		error("Sign term not found!");
    	}
    }
    
    /*END OF GRAMMAR AND BEGIN OPERATOR METHODS*/
    
    /**
     * Takes in the lookahead token and matches it to a relop.
     * @return 
     */
    public OperationNode relop()
    {
    	OperationNode answer = null;
    	if(lookahead.type == TokenType.EQUAL)
    	{
    		match(TokenType.EQUAL);
    		answer = new OperationNode(TokenType.EQUAL);
    	}
    	else if(lookahead.type == TokenType.EQUALCHECK)
    	{
    		match(TokenType.EQUALCHECK);
    		answer = new OperationNode(TokenType.EQUALCHECK);
    	}
    	else if(lookahead.type == TokenType.LESS_THAN)
    	{
    		match(TokenType.LESS_THAN);
    		answer = new OperationNode(TokenType.LESS_THAN);
    	}
    	else if(lookahead.type == TokenType.LESS_THAN_EQUAL)
    	{
    		match(TokenType.LESS_THAN_EQUAL);
    		answer = new OperationNode(TokenType.LESS_THAN_EQUAL);
    	}
    	else if(lookahead.type == TokenType.GREATER_THAN_EQUAL)
    	{
    		match(TokenType.GREATER_THAN_EQUAL);
    		answer = new OperationNode(TokenType.GREATER_THAN_EQUAL);
    	}
    	else if(lookahead.type == TokenType.GREATER_THAN)
		{
    		match(TokenType.GREATER_THAN);
    		answer = new OperationNode(TokenType.GREATER_THAN);
		}
    	else
    	{
    		error("Non Relop token found!");
    	}
    	return answer;
    }
    
    /**
     * Takes in the lookahead token and matches it to a addop.
     */
    public OperationNode addop()
    {
    	OperationNode answer = null;
    	if(lookahead.type == TokenType.PLUS)
    	{
    		match(TokenType.PLUS);
    		answer = new OperationNode(TokenType.PLUS);
    	}
    	else if(lookahead.type == TokenType.MINUS)
    	{
    		match(TokenType.MINUS);
    		answer = new OperationNode(TokenType.MINUS);
    	}
    	else if(lookahead.type == TokenType.OR)
    	{
    		match(TokenType.OR);
    		answer = new OperationNode(TokenType.OR);
    	}
    	else
    	{
    		error("Non Addop token found!");
    	}
    	return answer;
    }
    
    /**
     * Takes in the lookahead token and matches it to a mulop.
     * @return 
     */
    public OperationNode mulop()
    {
    	OperationNode answer = null;
    	if(lookahead.type == TokenType.MULTIPLY)
    	{
    		match(TokenType.MULTIPLY);
    		answer = new OperationNode(TokenType.MULTIPLY);
    	}
    	else if(lookahead.type == TokenType.DIVIDE)
    	{
    		match(TokenType.DIVIDE);
    		answer = new OperationNode(TokenType.DIVIDE);
    	}
    	else if(lookahead.type == TokenType.DIV)
    	{
    		match(TokenType.DIV);
    		answer = new OperationNode(TokenType.DIV);
    	}
    	else if(lookahead.type == TokenType.MODULUS)
    	{
    		match(TokenType.MODULUS);
    		answer = new OperationNode(TokenType.MODULUS);
    	}
    	else if(lookahead.type == TokenType.AND)
    	{
    		match(TokenType.AND);
    		answer = new OperationNode(TokenType.AND);
    	}
    	else
    	{
    		error("Non Mulop token found!");
    	}
    	return answer;
    }
    
    /**
     * Checks if a token is of relop type.
     * @param token The token to be checked.
     * @return True if the token is a relop, false otherwise.
     */
    public boolean isRelop(Token token)
    {
    	if(token.type == TokenType.EQUAL
    	||	token.type == TokenType.EQUALCHECK
    	||	token.type == TokenType.LESS_THAN
    	||	token.type == TokenType.LESS_THAN_EQUAL
    	||	token.type == TokenType.GREATER_THAN_EQUAL
    	||	token.type == TokenType.GREATER_THAN
    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * Checks if a token is of addop type.
     * @param token The token to be checked.
     * @return True if the token is a addop, false otherwise.
     */
    public boolean isAddop(Token token)
    {
    	if(token.type == TokenType.PLUS
    	||	token.type == TokenType.MINUS
    	||	token.type == TokenType.OR
    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * Checks if a token is of mulop type.
     * @param token The token to be checked.
     * @return True if the token is a mulop, false otherwise.
     */
    public boolean isMulop(Token token)
    {
    	if(token.type == TokenType.MULTIPLY
    	||	token.type == TokenType.DIVIDE
    	||	token.type == TokenType.DIV
    	||	token.type == TokenType.MODULUS
    	||	token.type == TokenType.AND
    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * This is an error function that throws an unchecked runtime exeption.
     * @param message The message that the method wants to pass to the error.
     * @throws RuntimeException Throws a runtime exeption with a given messeage.
     */
    public void error(String message) throws RuntimeException
    {
        String errormsg = "Error " + message + " at line " + 
                this.scanner.getLine() + " column " + 
                this.scanner.getColumn();
        throw new RuntimeException(errormsg);
    }
}
