/**
 * Recognizer.java
 * Compilers 2
 * February 24, 2019
 * This code is used to read in a string or file that contains pascal code and checks to make sure it 
 * is in the correct format.
 * @author Nghia Huynh
 * @version 2
 */
package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import analyzer.DataType;
import scanner.*;
import symboltable.*;

public class Recognizer {

    /**
     * This a private token object
     */
    private Token lookahead;
    
    /**
     * This a private scanner object.
     */
    private TokenScanner scanner;
    
    /**
     * This is a private symboletable object
     */
    private SymbolTable table = new SymbolTable();
     
    /**
     * This the constructor for the recognizer class
     * takes in file or string and passes it the scanner.
     * @param text Takes and input of either a string or file.
     */
    public Recognizer(String text)
    {
    	//This line checks to see if the string ends with the pascal file type
    	//If it is true then we are dealing with a file.
    if( text.endsWith(".pas"))
    {
    	//This block is used to try and read the contents of the given file.
    	scanner = null;
    	try
    	{
            FileInputStream stream = new java.io.FileInputStream(text);
            InputStreamReader reader = new java.io.InputStreamReader(stream);
            scanner = new TokenScanner(reader);
    	}
    	catch(FileNotFoundException e)
    	{
    		error("No File Was Found!");
    	}
    }
    //If the above check fails then it will try to run the recognizer on the given string.
    else
    {
    	scanner = new TokenScanner(text);
    }
    //Try to grab next token if not then code shuts down.
    try 
    {
    	lookahead = scanner.nextToken();
    }
    catch (IOException e)
    {
    	error("Could not scan next token!");
    }
    }
    
    /**
     * This is a matching function that checks if the lookahead token is what we are looking for.
     * @param expected
     */
    public void match(TokenType expected)
    {
    	//Check to see if the next token is the correct one and if yes then call next token.
    	if(this.lookahead.type == expected)
    	{
    		try
    		{
    			this.lookahead = scanner.nextToken();
    			//Checks for end of the file.
    			if(this.lookahead == null)
    			{
    				this.lookahead = new Token("End of file!", null);
    			}
    		}
    		catch (IOException e)
    		{
    			error("Could not scan next token!");
    		}
    	}
    	else
    	{
    		error("Expected token of type " + expected + " , found a token of type " + this.lookahead.type + " instead!");
    	}
    }
    
    /**
     * NOTE: DO NOT USE THIS IS NOT A REGULAR PART OF THE RECOGNIZER IT IS FOR TESTING PURPOSES ONLY!
     * This adds an ID into the internal symboltable table object.
	 * @param newname	name for a new symbol
	 * @param newkind	kind for a new symbol
	 * @param newtype	type for a new symbol
     */
    public void addID(String newname, KindType newkind, DataType newtype)
    {
    	table.addSymbol(newname, newkind, newtype);;
    }
    
    /** Takes the tostring function from the SymbolTable.java and allows the recognizer to call this
     * and return its own clone of that toString.
     */
    public String toString()
    {
		return table.toString();
    }
    
    /*END OF FUNCTIONS AND BEGIN GRAMMAR*/
    
    /**
     * This is the program grammer and is what is first called to start the recognizer.
     */
    public void program()
    {
    	match(TokenType.PROGRAM);
    	table.addSymbol(lookahead.lexeme, KindType.PROGRAM, null);
    	match(TokenType.ID);
    	match(TokenType.SEMICOLON);
    	declarations();
    	subprogramDeclarations();
    	compoundStatement();
    	match(TokenType.PERIOD);
    }
    
    /**
     * Checks for a list of identifiers.
     */
    public void identifierList()
    {
    	table.addSymbol(lookahead.lexeme, KindType.VARIABLE, null);
		match(TokenType.ID);
		if(lookahead.type == TokenType.COMMA)
		{
			match(TokenType.COMMA);
			identifierList();
		}
    }
    
    /**
     * Checks for the declarations within the programs.
     */
    public void declarations()
    {
    	if(lookahead.type == TokenType.VARIABLE)
    	{
    		match(TokenType.VARIABLE);
    		identifierList();
    		match(TokenType.COLON);
    		type();
    		match(TokenType.SEMICOLON);
    		declarations();
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * Checks the type of a part  of the code.
     */
    public void type()
    {
    	if(lookahead.type == TokenType.ARRAY)
    	{
    		match(TokenType.ARRAY);
    		match(TokenType.LEFT_BRACKET);
    		match(TokenType.NUMBER);
    		match(TokenType.COLON);
    		match(TokenType.NUMBER);
    		match(TokenType.RIGHT_BRACKET);
    		match(TokenType.OF);
    		standardType();
    	}
    	else
    	{
    		standardType();
    	}
    }
    
    /**
     * Checks of the string is a standard type. Either and int or a real.
     */
    public void standardType()
    {
    	if(lookahead.type == TokenType.INTEGER)
    	{
    		match(TokenType.INTEGER);
    	}
    	else if(lookahead.type == TokenType.REAL)
    	{
    		match(TokenType.REAL);
    	}
    	else
    	{
    		error("Standard term not found!");
    	}
    }
    
    /**
     * checks if a subprogram is being called by seeing if the keyword has been chosen.
     */
    public void subprogramDeclarations()
    {
    	if(lookahead.type == TokenType.FUNCTION || lookahead.type == TokenType.PROCEDURE)
    	{
    		subprogramDeclaration();
    		match(TokenType.SEMICOLON);
    		subprogramDeclarations();
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * Declares a subprogram structure.
     */
    public void subprogramDeclaration()
    {
    	subprogramHead();
    	declarations();
    	compoundStatement();
    }
    
    /**
     * Start of the subprogram structure.
     */
    public void subprogramHead()
    {
    	if(lookahead.type == TokenType.FUNCTION)
    	{
    		match(TokenType.FUNCTION);
        	table.addSymbol(lookahead.lexeme, KindType.FUNCTION, null);
    		match(TokenType.ID);
    		arguments();
    		match(TokenType.COLON);
    		standardType();
    		match(TokenType.SEMICOLON);
    	}
    	else if(lookahead.type == TokenType.PROCEDURE)
    	{
    		match(TokenType.PROCEDURE);
        	table.addSymbol(lookahead.lexeme, KindType.PROCEDURE, null);
    		match(TokenType.ID);
    		arguments();
    		match(TokenType.SEMICOLON);
    	}
    	else
    	{
    		error("Subprogram head term not found!");
    	}
    }
    
    /**
     * Checks if an argument is being added to the code
     */
    public void arguments()
    {
    	if(lookahead.type == TokenType.LEFT_PARENTHESIS)
    	{
    		match(TokenType.LEFT_PARENTHESIS);
    		parameterList();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * Adds parameters to the system and if it sees a semi colon it adds some more.
     */
    public void parameterList()
    {
    	identifierList();
    	match(TokenType.COLON);
    	type();
    	if(lookahead.type == TokenType.SEMICOLON)
    	{
    		match(TokenType.SEMICOLON);
    		parameterList();
    	}
    }
    
    /**
     * This is the beginning of a compound statement.
     */
    public void compoundStatement()
    {
    	match(TokenType.BEGIN);
    	optionalStatements();
    	match(TokenType.END);
    }
    
    /**
     * Checks if an optional statement is being called upon and if true it calls upon 
     * a statement list.
     */
    public void optionalStatements()
    {
    	if(lookahead.type == TokenType.ID
    		||	lookahead.type == TokenType.BEGIN
    		||	lookahead.type == TokenType.IF
    		||	lookahead.type == TokenType.WHILE
    		||	lookahead.type == TokenType.READ
    		||	lookahead.type == TokenType.WRITE
    		||	lookahead.type == TokenType.RETURN)
    	{
    		statementList();
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * This builds a list of statements that gets called again if a statement ends in a semi colon.
     */
    public void statementList()
    {
    	statement();
    	if(lookahead.type == TokenType.SEMICOLON)
    	{
    		match(TokenType.SEMICOLON);
    		statementList();
    	}
    }
    
    /**
     * This holds the structure for a statement.
     */
    public void statement()
    {
    	//Check if the look ahead is any of these types and if it is go down that tree
    	//NOTE: MISSING PROCEDURE STATEMENT CALL.
    	if(lookahead.type == TokenType.ID)
    	{
    		if(table.isVariable(lookahead.lexeme))
    		{
        		variable();
        		match(TokenType.ASSIGNOP);
        		expression();
    		}
    		else if(table.isProcedure(lookahead.lexeme))
    		{
    			procedureStatement();
    		}
    		else
    		{
    			error("Id is not of the correct kind!");
    		}
    	}
    	else if(lookahead.type == TokenType.BEGIN)
    	{
    		compoundStatement();
    	}
    	else if(lookahead.type == TokenType.IF)
    	{
    		match(TokenType.IF);
    		expression();
    		match(TokenType.THEN);
    		statement();
    		match(TokenType.ELSE);
    		statement();
    	}
    	else if(lookahead.type == TokenType.WHILE)
    	{
    		match(TokenType.WHILE);
    		expression();
    		match(TokenType.DO);
    		statement();
    	}
    	else if(lookahead.type == TokenType.READ)
    	{
    		match(TokenType.READ);
    		match(TokenType.LEFT_PARENTHESIS);
    		match(TokenType.ID);
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    	else if(lookahead.type == TokenType.WRITE)
    	{
    		match(TokenType.WRITE);
    		match(TokenType.LEFT_PARENTHESIS);
    		expression();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    	else if(lookahead.type == TokenType.RETURN)
    	{
    		match(TokenType.RETURN);
    		expression();
    	}
    	else
    	{
    		error("Statement error!");
    	}
    }

	/**
	 * Checks for a variable and adds an expression if a left bracket is found.
	 */
	public void variable()
    {
    	match(TokenType.ID);
    	if(lookahead.type == TokenType.LEFT_BRACKET)
    	{
    		match(TokenType.LEFT_BRACKET);
    		expression();
    		match(TokenType.RIGHT_BRACKET);
    	}
    }
    
    /**
     * Begins a procedure if an id is found and if its followed by a parenthesis then the 
     * code adds an expression.
     */
    public void procedureStatement()
    {
    	match(TokenType.ID);
    	if(lookahead.type == TokenType.LEFT_PARENTHESIS)
    	{
    		match(TokenType.LEFT_PARENTHESIS);
    		expressionList();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    }
    
    /**
     * Creates a list of expressions with a chain if the expression ends with a comma.
     */
    public void expressionList()
    {
    	expression();
    	if(lookahead.type == TokenType.COMMA)
    	{
    		match(TokenType.COMMA);
    		expressionList();
    	}
    }
    
    /**
     * A simple expression that checks if there is relop.
     */
    public void expression()
    {
    	simpleExpression();
    	if(isRelop(lookahead))
    	{
    		relop();
    		simpleExpression();
    	}
    }
    
    /**
     * simple expression is used to see the look ahead of the code.
     */
    public void simpleExpression()
    {
    	if(lookahead.type == TokenType.ID
    	||	lookahead.type == TokenType.NUMBER
    	||	lookahead.type == TokenType.LEFT_PARENTHESIS
    	||	lookahead.type == TokenType.NOT
    	)
    	{
    		term();
    		simplePart();
    	}
    	else if(lookahead.type == TokenType.PLUS
    	|| lookahead.type == TokenType.MINUS)
    	{
    		sign();
    		term();
    		simplePart();
    	}
    	else
    	{
    		error("Simple expression not found!");
    	}
    }
    
    /**
     * This checks for if there is an addop at the begining and used to add in a simple part.
     */
    public void simplePart()
    {
    	if(isAddop(lookahead))
    	{
    		addop();
    		term();
    		simplePart();
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * This represents a term within the grammar.
     */
    public void term()
    {
    	factor();
    	termPart();
    }
    
    /**
     * This sets the term part grammar.
     */
    public void termPart()
    {
    	if(isMulop(lookahead))
    	{
    		mulop();
    		factor();
    		termPart();
    	}
    	else
    	{
    		//Lambda
    	}
    }
    
    /**
     * Checks if the function is viable factor.
     */
    public void factor()
    {
    	if(lookahead.type == TokenType.ID)
    	{
    		match(TokenType.ID);
    		if(lookahead.type == TokenType.LEFT_BRACKET)
    		{
    			match(TokenType.LEFT_BRACKET);
    			expression();
    			match(TokenType.RIGHT_BRACKET);
    		}
    		else if(lookahead.type == TokenType.LEFT_PARENTHESIS)
    		{
    			match(TokenType.LEFT_PARENTHESIS);
    			expressionList();
    			match(TokenType.RIGHT_PARENTHESIS);
    		}
    	}
    	else if (lookahead.type == TokenType.NUMBER)
    	{
    		match(TokenType.NUMBER);
    	}
    	else if (lookahead.type == TokenType.LEFT_PARENTHESIS)
    	{
    		match(TokenType.LEFT_PARENTHESIS);
    		expression();
    		match(TokenType.RIGHT_PARENTHESIS);
    	}
    	else if (lookahead.type == TokenType.NOT)
    	{
    		match(TokenType.NOT);
    		factor();
    	}
    	else
    	{
    		error("Factor term not found!");
    	}
    }
    
    /**
     * Checks the sign of the function and see if its a plus or minus.
     */
    public void sign()
    {
    	if(lookahead.type == TokenType.PLUS)
    	{
    		match(TokenType.PLUS);
    	}
    	else if(lookahead.type == TokenType.MINUS)
    	{
    		match(TokenType.MINUS);
    	}
    	else
    	{
    		error("Sign term not found!");
    	}
    }
    
    /*END OF GRAMMAR AND BEGIN OPERATOR METHODS*/
    
    /**
     * Takes in the lookahead token and matches it to a relop.
     */
    public void relop()
    {
    	if(lookahead.type == TokenType.EQUAL)
    	{
    		match(TokenType.EQUAL);
    	}
    	else if(lookahead.type == TokenType.EQUALCHECK)
    	{
    		match(TokenType.EQUALCHECK);
    	}
    	else if(lookahead.type == TokenType.LESS_THAN)
    	{
    		match(TokenType.LESS_THAN);
    	}
    	else if(lookahead.type == TokenType.LESS_THAN_EQUAL)
    	{
    		match(TokenType.LESS_THAN_EQUAL);
    	}
    	else if(lookahead.type == TokenType.GREATER_THAN_EQUAL)
    	{
    		match(TokenType.GREATER_THAN_EQUAL);
    	}
    	else if(lookahead.type == TokenType.GREATER_THAN)
		{
    		match(TokenType.GREATER_THAN);
		}
    	else
    	{
    		error("Non Relop token found!");
    	}
    }
    
    /**
     * Takes in the lookahead token and matches it to a addop.
     */
    public void addop()
    {
    	if(lookahead.type == TokenType.PLUS)
    	{
    		match(TokenType.PLUS);
    	}
    	else if(lookahead.type == TokenType.MINUS)
    	{
    		match(TokenType.MINUS);
    	}
    	else if(lookahead.type == TokenType.OR)
    	{
    		match(TokenType.OR);
    	}
    	else
    	{
    		error("Non Addop token found!");
    	}
    }
    
    /**
     * Takes in the lookahead token and matches it to a mulop.
     */
    public void mulop()
    {
    	if(lookahead.type == TokenType.MULTIPLY)
    	{
    		match(TokenType.MULTIPLY);
    	}
    	else if(lookahead.type == TokenType.DIVIDE)
    	{
    		match(TokenType.DIVIDE);
    	}
    	else if(lookahead.type == TokenType.DIV)
    	{
    		match(TokenType.DIV);
    	}
    	else if(lookahead.type == TokenType.MODULUS)
    	{
    		match(TokenType.MODULUS);
    	}
    	else if(lookahead.type == TokenType.AND)
    	{
    		match(TokenType.AND);
    	}
    	else
    	{
    		error("Non Mulop token found!");
    	}
    }
    
    /**
     * Checks if a token is of relop type.
     * @param token The token to be checked.
     * @return True if the token is a relop, false otherwise.
     */
    public boolean isRelop(Token token)
    {
    	if(token.type == TokenType.EQUAL
    	||	token.type == TokenType.EQUALCHECK
    	||	token.type == TokenType.LESS_THAN
    	||	token.type == TokenType.LESS_THAN_EQUAL
    	||	token.type == TokenType.GREATER_THAN_EQUAL
    	||	token.type == TokenType.GREATER_THAN
    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * Checks if a token is of addop type.
     * @param token The token to be checked.
     * @return True if the token is a addop, false otherwise.
     */
    public boolean isAddop(Token token)
    {
    	if(token.type == TokenType.PLUS
    	||	token.type == TokenType.MINUS
    	||	token.type == TokenType.OR
    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * Checks if a token is of mulop type.
     * @param token The token to be checked.
     * @return True if the token is a mulop, false otherwise.
     */
    public boolean isMulop(Token token)
    {
    	if(token.type == TokenType.MULTIPLY
    	||	token.type == TokenType.DIVIDE
    	||	token.type == TokenType.DIV
    	||	token.type == TokenType.MODULUS
    	||	token.type == TokenType.AND
    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * This is an error function that throws an unchecked runtime exeption.
     * @param message The message that the method wants to pass to the error.
     * @throws RuntimeException Throws a runtime exeption with a given messeage.
     */
    public void error(String message) throws RuntimeException
    {
        String errormsg = "Error " + message + " at line " + 
                this.scanner.getLine() + " column " + 
                this.scanner.getColumn();
        throw new RuntimeException(errormsg);
    }
}
