package parser;

import static org.junit.Assert.*;

import org.junit.Test;

import analyzer.DataType;
import scanner.TokenType;
import symboltable.*;
import syntaxtree.*;

public class ParserTest {

	/**
	 * This tests to see if the Parser can successfully open a file and see if the 
	 * proper pascal program can be read from it
	 * fails if it fails to recognize the program.
	 * Also tests to see if the syntax tree is the correct shape.
	 */
	@Test
	public void testProgramGood() {
		String code = "./src/parser/testgoodcode.pas";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			String pnexspected = "Program: foo\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: INTEGER : fee\n" + 
					"|-- --- Name: INTEGER : fi\n" + 
					"|-- --- Name: INTEGER : fo\n" + 
					"|-- --- Name: INTEGER : fum\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : fee\n" + 
					"|-- --- --- Value: null : 4\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : fi\n" + 
					"|-- --- --- Value: null : 5\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : fo\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- --- --- Value: null : 3\n" + 
					"|-- --- --- --- --- --- --- Name: null : fee\n" + 
					"|-- --- --- --- --- Name: null : fi\n" + 
					"|-- --- If: null\n" + 
					"|-- --- --- Operation: null : LESS_THAN\n" + 
					"|-- --- --- --- --- Name: null : fo\n" + 
					"|-- --- --- --- --- Value: null : 13\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : fo\n" + 
					"|-- --- --- --- Value: null : 13\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : fo\n" + 
					"|-- --- --- --- Value: null : 26\n";
			assertEquals(pn.indentedToString(0),pnexspected);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize program!");
		}
	}
	
	/**
	 * This opens a pascal program and proceeds to read it
	 * Threre is a code error within the file and it shoudl return with the correct error.
	 */
	@Test
	public void testProgramBad() {
		String code = "./src/parser/testbadcode.pas";
		Parser recog = new Parser(code);
		try
		{
			recog.program();
			fail("Failed to recognize spagetti code!");
		}
		catch(RuntimeException e)
		{
			String expected = "Error Expected token of type END , found a token of type ID instead! at line 5 column 21";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}

	/**
	 * This checks if a declaration can properly be detected and fails otherwise.
	 */
	@Test
	public void testDeclarationsGood() {
		String code = "var fee, fi, fo, fum: integer;"
				+ " var banana, grapes, orange, potato: real;";
		Parser recog = new Parser(code);
		try
		{
			String actual = recog.declarations().indentedToString(0);
			String exspected = "Declarations\n" + 
					"|-- Name: INTEGER : fee\n" + 
					"|-- Name: INTEGER : fi\n" + 
					"|-- Name: INTEGER : fo\n" + 
					"|-- Name: INTEGER : fum\n" + 
					"|-- Name: REAL : banana\n" + 
					"|-- Name: REAL : grapes\n" + 
					"|-- Name: REAL : orange\n" + 
					"|-- Name: REAL : potato\n";
			assertEquals(exspected,actual);
		}
		catch(RuntimeException e)
		{
			fail("Failed to recognize declarations!");
		}
		
	}
	
	/**
	 * This checks if the program can recognize bad declarations
	 * instead of semicolon there is a comma, which should cause an error.
	 */
	@Test
	public void testDeclarationsBad() {
		String code = "var fee, fi, fo, fum: integer, var banana, grapes, orange, potato: real;";
		Parser recog = new Parser(code);
		try
		{
			recog.declarations();
			fail("Failed to recognize spagetti code!");
		}
		catch(RuntimeException e)
		{
			String expected = "Error Expected token of type SEMICOLON , found a token of type COMMA instead! at line 0 column 29";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}
	
	/**
	 * Checks if the code can handle a chain of subprogram declarations.
	 * Succeeds if the code can reconize the legal function.
	 */
	@Test
	public void testSubprogramDeclartionGood() {
		String code = "function max(num1, num2: integer): integer;"
				+ "var result: integer; "
				+ "begin if (num1 > num2) then result := num1 else result := num2; "
				+ "total := result end";
		Parser recog = new Parser(code);
		recog.addID("num1", KindType.VARIABLE, null);
		recog.addID("num2", KindType.VARIABLE, null);
		recog.addID("result", KindType.VARIABLE, null);
		recog.addID("total", KindType.VARIABLE, null);
		try
		{
			String actual = recog.subprogramDeclaration().indentedToString(0);
			String exspected = "SubProgramNode\n";
			assertEquals(exspected,actual);
			
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize subprogram declaration!");
		}
	}

	/**
	 * Checks for when there is bad code where a semicolon was expected, but there
	 * was a colon instead. If it works then it will recognize the bad code.
	 */
	@Test
	public void testSubprogramDecalartionBad() {
		String code = "function max(num1, num2: integer): integer; "
				+ "var result: integer: "
				+ "begin if (num1 > num2) then result num1 else result := num2; "
				+ "total := result end;";
		Parser recog = new Parser(code);
		recog.addID("num1", KindType.VARIABLE, null);
		recog.addID("num2", KindType.VARIABLE, null);
		recog.addID("result", KindType.VARIABLE, null);
		recog.addID("total", KindType.VARIABLE, null);
		try
		{
			recog.subprogramDeclaration();
			fail("Failed to recognize spagetti code!");
		}
		catch(RuntimeException e)
		{
			String expected = "Error Expected token of type SEMICOLON , found a token of type COLON instead! at line 0 column 63";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}
	
	/**
	 * Checks if a conditional statement is correctly written and 
	 * Fails if it is not recognized.
	 */
	@Test
	public void testStatementGood() {
		String code = "if num1 > num2 then result := num1 else result := num2";
		Parser recog = new Parser(code);
		recog.addID("num1", KindType.VARIABLE, null);
		recog.addID("num2", KindType.VARIABLE, null);
		recog.addID("result", KindType.VARIABLE, null);
		try
		{
			String actual = recog.statement().indentedToString(0);
			String exspected = "If: null\n" + 
					"|-- Operation: null : GREATER_THAN\n" + 
					"|-- --- --- Name: null : num1\n" + 
					"|-- --- --- Name: null : num2\n" + 
					"|-- Assignment: null\n" + 
					"|-- --- Name: null : result\n" + 
					"|-- --- Name: null : num1\n" + 
					"|-- Assignment: null\n" + 
					"|-- --- Name: null : result\n" + 
					"|-- --- Name: null : num2\n";
			assertEquals(exspected,actual);
			
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize statement declaration!");
		}
	}
	
	/**
	 * This checks for when there is a missing string within the code.
	 * If it detects the missing code then it has not failed.
	 */
	@Test
	public void testStatementBad() {
		String code = "if (num1 > num2) then := num1 else result := num2;";
		Parser recog = new Parser(code);
		recog.addID("num1", KindType.VARIABLE, null);
		recog.addID("num2", KindType.VARIABLE, null);
		recog.addID("result", KindType.VARIABLE, null);
		try
		{
			recog.statement();
			fail("Failed to recognize spagetti code!");
		}
		catch(RuntimeException e)
		{
			String expected = "Error Statement error! at line 0 column 22";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}

	/**
	 * Checks if a simple chained expression is working.
	 * It uses a mix of functions all combined to show how it can continue to show a correct path.
	 */
	@Test
	public void testSimpleExpressionGood() {
		String code = "a  - e1 + f1 * 3";
		Parser recog = new Parser(code);
		recog.addID("a", KindType.VARIABLE, null);
		recog.addID("e1", KindType.VARIABLE, null);
		recog.addID("f1", KindType.VARIABLE, null);
		try
		{
			String actual = recog.simpleExpression().indentedToString(0);
			String exspected = "Operation: null : PLUS\n" + 
					"|-- --- Operation: null : MINUS\n" + 
					"|-- --- --- --- Name: null : a\n" + 
					"|-- --- --- --- Name: null : e1\n" + 
					"|-- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- Name: null : f1\n" + 
					"|-- --- --- --- Value: null : 3\n";
			assertEquals(exspected,actual);
		}
		catch(RuntimeException e)
		{
			fail("Failed to recognize subprogram declaration!");
		}
	}
	
	/**
	 * Tests if there are too many symbols and not enough ids to fill them causisng an error.
	 */
	@Test
	public void testSimpleExpressionBad() {
		String code = "a  - + e1 + f1";
		Parser recog = new Parser(code);
		recog.addID("a", KindType.VARIABLE, null);
		recog.addID("e1", KindType.VARIABLE, null);
		recog.addID("f1", KindType.VARIABLE, null);
		try
		{
			recog.simpleExpression();
			fail("Failed to recognize spagetti code!");
		}
		catch(RuntimeException e)
		{
			String expected = "Error Factor term not found! at line 0 column 5";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}

	/**
	 * Test a factor element to see if the code can be recognize
	 * and fails otherwise.
	 */
	@Test
	public void testFactorGood() {
		String code = "10000";
		Parser recog = new Parser(code);
		try
		{
			String actual = recog.factor().indentedToString(0);
			String exspected = "Value: null : 10000\n";
			assertEquals(exspected,actual);
		}
		catch(RuntimeException e)
		{
			fail("Failed to recognize factor declaration!");
		}
	}
	
	/**
	 *This string has an extra factor that is not connected to any addop or mulop.
	 *The code should fail if it sees this.
	 */
	@Test
	public void testFactorBad() {
		String code = "apple (a  - e1 + f1 * 3 3 )";
		Parser recog = new Parser(code);
		recog.addID("apple", KindType.VARIABLE, null);
		try
		{
			recog.factor();
			fail("Failed to recognize spagetti code!");
			
		}
		catch(RuntimeException e)
		{
			String expected = "Error a: This is not a variable! at line 0 column 7";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}
	
	/**
	 * This test is used to make sure that an assignment function works as we add more expressions.
	 */
	@Test
	public void testCompoundStatementGood()
	{
		String code = "begin  dollars := 1000000 + 0;" + 
				"  yen := dollars * 110;" +
				"bitcoins := dollars / 3900 end";
		Parser recog = new Parser(code);
		recog.addID("dollars", KindType.VARIABLE, null);
		recog.addID("yen", KindType.VARIABLE, null);
		recog.addID("bitcoins", KindType.VARIABLE, null);
		
		try
		{
			String actual = recog.compoundStatement().indentedToString(0);
			String exspected = "Compound Statement\n" + 
					"|-- Assignment: null\n" + 
					"|-- --- Name: null : dollars\n" + 
					"|-- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- Value: null : 1000000\n" + 
					"|-- --- --- --- Value: null : 0\n" + 
					"|-- Assignment: null\n" + 
					"|-- --- Name: null : yen\n" + 
					"|-- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- Value: null : 110\n" + 
					"|-- Assignment: null\n" + 
					"|-- --- Name: null : bitcoins\n" + 
					"|-- --- Operation: null : DIVIDE\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- Value: null : 3900\n";
			assertEquals(exspected,actual);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize an assignment declaration!");
		}
	}
	
	/**
	 * This tests to see if the program can catch if a an variable iD is used as a procedure ID
	 * which means it should error because it is looking for an assignop.
	 */
	@Test
	public void testCompoundStatementBad()
	{
		String code = "begin Orange := 1 + 2; potato := 1 + 5; apple(5 + 5) end";
		Recognizer recog = new Recognizer(code);
		recog.addID("Orange", KindType.VARIABLE, null);
		recog.addID("potato", KindType.PROCEDURE, null);
		recog.addID("apple", KindType.PROCEDURE, null);
		try
		{
			recog.compoundStatement();
			fail("Recognized wrong assigned kind type!");
		}
		catch(RuntimeException e)
		{
			//NOTE:The statement list function dosent call itself for apple because the token after potato was
			//not a semicolon(it was the assignop) so it ended the statement chain which ended the compound
			//statement which is why its looking for the END token.
			String expected = "Error Expected token of type END , found a token of type ASSIGNOP instead! at line 0 column 30";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}
	
	/**
	 * This test is used to test a list of procedure statements ones after another. It also makes
	 * sure that the correct statement switch is being triggered.
	 */
	@Test
	public void testProcedureGood()
	{
		String code = "potato(1 + 2, 5 < 8); apple(1 -2, 5*6, 90 + 4) ";
		Parser recog = new Parser(code);
		recog.addID("potato", KindType.PROCEDURE, null);
		recog.addID("apple", KindType.PROCEDURE, null);
		try
		{
			recog.statementList();
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize a procedure declaration!");
		}
	}
	
	/**
	 * This test is to check if using a variable ID can be caught by the Parser. If it is caught being 
	 * used as a procedure call.
	 */
	@Test
	public void testProcedureBad()
	{
		String code = "potato(1 + 2, 5 < 8); apple(1 -2, 5*6, 90 + 4)";
		Parser recog = new Parser(code);
		recog.addID("potato", KindType.PROCEDURE, null);
		recog.addID("apple", KindType.VARIABLE, null);
		try
		{
			recog.statementList();
			fail("Failed to recognize a variable declaration inside of a procedure!");
		}
		catch(RuntimeException e)
		{
			String expected = "Error Expected token of type ASSIGNOP , found a token of type LEFT_PARENTHESIS instead! at line 0 column 27";
			String actual = e.getMessage();
			assertEquals(expected,actual);
		}
	}
	
	
	
	/**
	 * Tests to see if the to string method is outputting the correct format for the table.
	 * Also to check if the table is being loaded with the correct information.
	 */
	@Test
	public void testToString()
	{
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("pineapple", KindType.PROCEDURE, DataType.INTEGER);
		testtable.addSymbol("greekyogurt", KindType.VARIABLE, DataType.INTEGER);
		testtable.addSymbol("pickle", KindType.FUNCTION, DataType.INTEGER);
		testtable.addSymbol("melon", KindType.PROGRAM, DataType.INTEGER);
		String exspected = String.format(" %-20s", "Symbol") + 
							String.format(" %-20s", "Kind") + 
							String.format(" %-20s", "Type") + "\n";
		exspected = exspected + "----------------------------------------------------------------\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","pineapple", KindType.PROCEDURE, DataType.INTEGER) + "\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","greekyogurt", KindType.VARIABLE, DataType.INTEGER) + "\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","melon", KindType.PROGRAM, DataType.INTEGER) + "\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","pickle", KindType.FUNCTION, DataType.INTEGER) + "\n";
		assertEquals(exspected, testtable.toString());
		
	}
	
	@Test
	public void testCoinExample() {
		String code = "./src/parser/money.pas";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			String exspected = 
					"Program: sample\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: INTEGER : dollars\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Name: INTEGER : bitcoins\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : dollars\n" + 
					"|-- --- --- Value: null : 1000000\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 110\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- --- Operation: null : DIVIDE\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 3900\n";
			assertEquals(pn.indentedToString(0),exspected);
		}
		catch(RuntimeException e)
		{
//			System.out.println(e.getMessage());
			fail("Failed to recognize program!");
		}
	}
}

