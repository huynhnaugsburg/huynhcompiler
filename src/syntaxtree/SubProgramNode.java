/**
 * SubProgramNode.java
 * Compilers 2
 * February 24, 2019
 * This is code for a subprogram node.
 * is in the correct format.
 * @author Nghia Huynh
 * @version 2
 */
package syntaxtree;

public class SubProgramNode extends SubProgramDeclarationsNode {

    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "SubProgramNode\n";
        return answer;
    }
}
