package syntaxtree;

/**
 * @author Nghia Huynh
 *
 */
public class WhileDoStatementNode extends StatementNode{
    
    private ExpressionNode expression;
    private StatementNode statement;

    public ExpressionNode getExpression() {
        return expression;
    }

    public void setExpression(ExpressionNode expression) {
        this.expression = expression;
    }
    
    public StatementNode getStatement() {
        return statement;
    }

    public void setStatement(StatementNode statement) {
        this.statement = statement;
    }
    

    
    /**
     * Creates a String representation of this assignment statement node 
     * and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "While: " + super.getDataType() + "\n";
        answer += this.expression.indentedToString( level + 1);
        answer += this.statement.indentedToString( level + 1);
        return answer;
    }
}
