
package syntaxtree;

import analyzer.DataType;

public abstract class StatementNode extends SyntaxTreeNode {
	private DataType type = null;
	
    public DataType getDataType() {
        return type;
    }

    public void setDataType(DataType type) {
        this.type = type;
    }
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Name: " + this.type + "\n";
        return answer;
    }
}
