package syntaxtree;

/**
 * @author Nghia Huynh
 *
 */
public class ReadStatementNode extends StatementNode{
    
	private String name;

    public ReadStatementNode( String attr) {
        this.name = attr;
    }
    
    public String getName() { return( this.name);}
    

    
    /**
     * Creates a String representation of this assignment statement node 
     * and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Read: " + super.getDataType() + " : " +  this.name + "\n";
        return answer;
    }
}
