package syntaxtree;

/**
 * Represents a single assignment statement.
 * @author Nghia Huynh
 */
public class WriteStatementNode extends StatementNode {
    
    private ExpressionNode expression;

    public ExpressionNode getExpression() {
        return expression;
    }

    public void setExpression(ExpressionNode expression) {
        this.expression = expression;
    }
    

    
    /**
     * Creates a String representation of this assignment statement node 
     * and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Write: " + super.getDataType() + "\n";
        answer += this.expression.indentedToString( level + 1);
        return answer;
    }
}