package syntaxtree;

import analyzer.DataType;

public abstract class ExpressionNode extends SyntaxTreeNode {
	
	private DataType type = null;
	
    public DataType getDataType() {
        return type;
    }

    public void setDataType(DataType type) {
        this.type = type;
    }
}
