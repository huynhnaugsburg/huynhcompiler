package syntaxtree;

public class ArrayNode extends VariableNode{

	private int offset;
	private int size;
	private ExpressionNode expression = null;
	
	public ArrayNode(String name)
	{
		super(name);
		this.offset =  0;
		this.size =  0;
			// TODO Auto-generated constructor stub
	}
	
	/** Sets the expression for the node
	 * @param node the node to be set
	 */
	public void setExpression(ExpressionNode node)
	{
		this.expression = node;
	}
	
	/**
	 * Gets the expression node
	 * @return
	 */
	public ExpressionNode getExpression()
	{
		return this.expression;
	}
	
    /* (non-Javadoc)
     * @see syntaxtree.VariableNode#getName()
     */
    public String getName()
    {
    	return this.name;
    }
    
    /**
     * Takes in a start and end of array and gets the size of the array
     * @param start The staring index
     * @param end the ending index
     */
    public void setSize(String start, String end)
    {
    	this.size = (Integer.parseInt(end) - Integer.parseInt(start)) + 1 ;;
    }
    
    /**
     * Returns the size of of the array.
     * @return
     */
    public int getSize()
    {
    	return this.size;
    }
    
    /**
     * This sets the offset of the array
     * @param offset
     */
    public void setOffSet(String offset)
    {
    	this.offset = Integer.parseInt(offset) * -1;
    }
    
    /**
     * This creates an array for system
     * @return
     */
    public int getOffSet()
    {
    	return this.offset;
    }
    
    
    /**
     * Creates a String representation of this variable node.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Name: " + super.getDataType() + " : " + this.name + "\n";
        answer += this.indentation(level) + "Size: " + this.size + "\n";
        answer += this.indentation(level) + "Offset: " + this.offset + "\n";
        if(expression != null)
        {
            answer += this.expression.indentedToString( level + 1);
        }
        return answer;
    }

    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof VariableNode) {
            VariableNode other = (VariableNode)o;
            if( this.name.equals( other.name)) answer = true;
        }
        return answer;
    }


}
