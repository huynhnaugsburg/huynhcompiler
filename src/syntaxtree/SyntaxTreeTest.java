/**
 * SyntaxTreeTest.java
 * Compilers 2
 * February 24, 2019
 * This code is to test the systems for adding nodes to the syntax tree.
 * is in the correct format.
 * @author Nghia Huynh
 * @version 2
 */
package syntaxtree;

import static org.junit.Assert.*;

import org.junit.Test;

import scanner.TokenType;

public class SyntaxTreeTest {

	/**
	 * This test creates a syntax tree manually using code from the syntax tree package.
	 * Then compares the output to an expsected string of what the tree should look like.
	 * If it matches then the code works to make the correct tree.
	 */
	@Test
	public void testSyntaxTree() {	
		//Below is code to make the tree.
		//Begin with program node.
		ProgramNode pn = new ProgramNode("sample");
		//This part adds stuff to the declarations part of the pn node.
		DeclarationsNode dn = new DeclarationsNode(); 
		VariableNode varn1 = new VariableNode("dollars");
		VariableNode varn2 = new VariableNode("yen");
		VariableNode varn3 = new VariableNode("bitcoins");
		dn.addVariable(varn1);
		dn.addVariable(varn2);
		dn.addVariable(varn3);
		pn.setVariables(dn);
		//This adds stuff to the subprogram declarations of the pn node.
		SubProgramDeclarationsNode sdn = new SubProgramDeclarationsNode();
		pn.setFunctions(sdn);
	
		//This parts add the compound statement node to the pn node.
		CompoundStatementNode csn = new CompoundStatementNode();
	
		AssignmentStatementNode asn1 = new AssignmentStatementNode();
		VariableNode varn4 = new VariableNode("dollars");
		ValueNode valn1 = new ValueNode("1000000");
		asn1.setLvalue(varn4);
		asn1.setExpression(valn1);
		
		AssignmentStatementNode asn2 = new AssignmentStatementNode();
		VariableNode varn5 = new VariableNode("yen");
		OperationNode on1 = new OperationNode(TokenType.MULTIPLY);
		VariableNode varn6 = new VariableNode("dollars");
		ValueNode valn2 = new ValueNode("102");
		on1.setLeft(varn6);
		on1.setRight(valn2);
		asn2.setLvalue(varn5);
		asn2.setExpression(on1);
		
		AssignmentStatementNode asn3 = new AssignmentStatementNode();
		VariableNode varn7 = new VariableNode("bitcoins");
		OperationNode on2 = new OperationNode(TokenType.DIVIDE);
		VariableNode varn8 = new VariableNode("dollars");
		ValueNode valn3 = new ValueNode("400");
		on2.setLeft(varn8);
		on2.setRight(valn3);
		asn3.setLvalue(varn7);
		asn3.setExpression(on2);
//		
		csn.addStatement(asn1);
		csn.addStatement(asn2);
		csn.addStatement(asn3);
		pn.setMain(csn);;
		//This is the expscted string that should be created by the syntax tree.
		String exspectedstring = "Program: sample\n" + 
				"|-- Declarations\n" + 
				"|-- --- Name: null : dollars\n" + 
				"|-- --- Name: null : yen\n" + 
				"|-- --- Name: null : bitcoins\n" + 
				"|-- SubProgramDeclarations\n" + 
				"|-- Compound Statement\n" + 
				"|-- --- Assignment: null\n" + 
				"|-- --- --- Name: null : dollars\n" + 
				"|-- --- --- Value: null : 1000000\n" + 
				"|-- --- Assignment: null\n" + 
				"|-- --- --- Name: null : yen\n" + 
				"|-- --- --- Operation: null : MULTIPLY\n" + 
				"|-- --- --- --- --- Name: null : dollars\n" + 
				"|-- --- --- --- --- Value: null : 102\n" + 
				"|-- --- Assignment: null\n" + 
				"|-- --- --- Name: null : bitcoins\n" + 
				"|-- --- --- Operation: null : DIVIDE\n" + 
				"|-- --- --- --- --- Name: null : dollars\n" + 
				"|-- --- --- --- --- Value: null : 400\n";
		assertEquals(pn.indentedToString(0), exspectedstring);
	}

}
