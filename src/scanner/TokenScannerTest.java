/**
 * TokenScannerTest.java
 * Compilers 2
 * January 25, 2018
 * This tests the TokenScanner and makes sure all of the patterns are recognized.
 * @author Nghia Huynh
 * @version 1
 */
package scanner;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

/**
 * @author K-money
 *
 */
public class TokenScannerTest {

	/**
	 * This checks to see if some words that are not key are properly being turned into ID tokens.
	 * @throws IOException
	 */
	@Test
	public void tokentest() throws IOException {
        TokenScanner scanner = null;
        scanner = new TokenScanner("Apples BANANA");
          Token apple = new Token("Apples",TokenType.ID);
          Token banana = new Token("BANANA", TokenType.ID);
		  Token token1 = scanner.nextToken();
		  Token token2 = scanner.nextToken();
		  assertEquals(apple.lexeme,token1.lexeme);
		  assertEquals(apple.type,token1.type);
		  assertEquals(banana.lexeme,token2.lexeme);
		  assertEquals(banana.type,token2.type);
	}
	
	/**
	 * This is a test to scan in a list of key words and making sure that they are correctly identified and give a correct token type.
	 * @throws IOException
	 */
	@Test
	public void keywordtest() throws IOException {
        TokenScanner scanner = null;
          scanner = new TokenScanner(" integer real while do not and array begin else end if of or program procedure then var function div mod read write return");
          //List of actual tokens that are needed from the string above.
          Token exspectedtoken1 = new Token("integer",TokenType.INTEGER);
          Token exspectedtoken2 = new Token("real",TokenType.REAL);
          Token exspectedtoken3 = new Token("while",TokenType.WHILE);
          Token exspectedtoken4 = new Token("do",TokenType.DO);
          Token exspectedtoken5 = new Token("not",TokenType.NOT);
          Token exspectedtoken6 = new Token("and",TokenType.AND);
          Token exspectedtoken7 = new Token("array",TokenType.ARRAY);
          Token exspectedtoken8 = new Token("begin",TokenType.BEGIN);
          Token exspectedtoken9 = new Token("else",TokenType.ELSE);
          Token exspectedtoken10 = new Token("end",TokenType.END);
          Token exspectedtoken11 = new Token("if",TokenType.IF);
          Token exspectedtoken12 = new Token("of",TokenType.OF);
          Token exspectedtoken13 = new Token("or",TokenType.OR);
          Token exspectedtoken14 = new Token("program",TokenType.PROGRAM);
          Token exspectedtoken15 = new Token("procedure",TokenType.PROCEDURE);
          Token exspectedtoken16 = new Token("then",TokenType.THEN);
          Token exspectedtoken17 = new Token("var",TokenType.VARIABLE);
          Token exspectedtoken18 = new Token("function",TokenType.FUNCTION);
          Token exspectedtoken19 = new Token("div",TokenType.DIV);
          Token exspectedtoken20 = new Token("mod",TokenType.MODULUS);
          Token exspectedtoken21 = new Token("read",TokenType.READ);
          Token exspectedtoken22 = new Token("write",TokenType.WRITE);
          Token exspectedtoken23 = new Token("return",TokenType.RETURN);
          //Scanning each individual token from the string.
          Token actualtoken1 = scanner.nextToken();
          Token actualtoken2 = scanner.nextToken();
          Token actualtoken3 = scanner.nextToken();
          Token actualtoken4 = scanner.nextToken();
          Token actualtoken5 = scanner.nextToken();
          Token actualtoken6 = scanner.nextToken();
          Token actualtoken7 = scanner.nextToken();
          Token actualtoken8 = scanner.nextToken();
          Token actualtoken9 = scanner.nextToken();
          Token actualtoken10 = scanner.nextToken();
          Token actualtoken11 = scanner.nextToken();
          Token actualtoken12 = scanner.nextToken();
          Token actualtoken13 = scanner.nextToken();
          Token actualtoken14 = scanner.nextToken();
          Token actualtoken15 = scanner.nextToken();
          Token actualtoken16 = scanner.nextToken();
          Token actualtoken17 = scanner.nextToken();
          Token actualtoken18 = scanner.nextToken();
          Token actualtoken19 = scanner.nextToken();
          Token actualtoken20 = scanner.nextToken();
          Token actualtoken21 = scanner.nextToken();
          Token actualtoken22 = scanner.nextToken();
          Token actualtoken23 = scanner.nextToken();
          //Asserts used to check if the exspected tokens are the same as the scanned ones.
		  assertEquals(exspectedtoken1.lexeme,actualtoken1.lexeme);
		  assertEquals(exspectedtoken1.type,actualtoken1.type);
		  assertEquals(exspectedtoken2.lexeme,actualtoken2.lexeme);
		  assertEquals(exspectedtoken2.type,actualtoken2.type);
		  assertEquals(exspectedtoken3.lexeme,actualtoken3.lexeme);
		  assertEquals(exspectedtoken3.type,actualtoken3.type);
		  assertEquals(exspectedtoken4.lexeme,actualtoken4.lexeme);
		  assertEquals(exspectedtoken4.type,actualtoken4.type);
		  assertEquals(exspectedtoken5.lexeme,actualtoken5.lexeme);
		  assertEquals(exspectedtoken5.type,actualtoken5.type);
		  assertEquals(exspectedtoken6.lexeme,actualtoken6.lexeme);
		  assertEquals(exspectedtoken6.type,actualtoken6.type);
		  assertEquals(exspectedtoken7.lexeme,actualtoken7.lexeme);
		  assertEquals(exspectedtoken7.type,actualtoken7.type);
		  assertEquals(exspectedtoken8.lexeme,actualtoken8.lexeme);
		  assertEquals(exspectedtoken8.type,actualtoken8.type);
		  assertEquals(exspectedtoken9.lexeme,actualtoken9.lexeme);
		  assertEquals(exspectedtoken9.type,actualtoken9.type);
		  assertEquals(exspectedtoken10.lexeme,actualtoken10.lexeme);
		  assertEquals(exspectedtoken10.type,actualtoken10.type);
		  assertEquals(exspectedtoken11.lexeme,actualtoken11.lexeme);
		  assertEquals(exspectedtoken11.type,actualtoken11.type);
		  assertEquals(exspectedtoken12.lexeme,actualtoken12.lexeme);
		  assertEquals(exspectedtoken12.type,actualtoken12.type);
		  assertEquals(exspectedtoken13.lexeme,actualtoken13.lexeme);
		  assertEquals(exspectedtoken13.type,actualtoken13.type);
		  assertEquals(exspectedtoken14.lexeme,actualtoken14.lexeme);
		  assertEquals(exspectedtoken14.type,actualtoken14.type);
		  assertEquals(exspectedtoken15.lexeme,actualtoken15.lexeme);
		  assertEquals(exspectedtoken15.type,actualtoken15.type);
		  assertEquals(exspectedtoken16.lexeme,actualtoken16.lexeme);
		  assertEquals(exspectedtoken16.type,actualtoken16.type);
		  assertEquals(exspectedtoken17.lexeme,actualtoken17.lexeme);
		  assertEquals(exspectedtoken17.type,actualtoken17.type);
		  assertEquals(exspectedtoken18.lexeme,actualtoken18.lexeme);
		  assertEquals(exspectedtoken18.type,actualtoken18.type);
		  assertEquals(exspectedtoken19.lexeme,actualtoken19.lexeme);
		  assertEquals(exspectedtoken19.type,actualtoken19.type);
		  assertEquals(exspectedtoken20.lexeme,actualtoken20.lexeme);
		  assertEquals(exspectedtoken20.type,actualtoken20.type);
		  assertEquals(exspectedtoken21.lexeme,actualtoken21.lexeme);
		  assertEquals(exspectedtoken21.type,actualtoken21.type);
		  assertEquals(exspectedtoken22.lexeme,actualtoken22.lexeme);
		  assertEquals(exspectedtoken22.type,actualtoken22.type);
		  assertEquals(exspectedtoken23.lexeme,actualtoken23.lexeme);
		  assertEquals(exspectedtoken23.type,actualtoken23.type); 
	}
	
	/**
	 * This test is for checking the symbols that are scanned in and to see if they output the corrent tokens.
	 * @throws IOException
	 */
	@Test
	public void symboltest() throws IOException {
        TokenScanner scanner = null;
          scanner = new TokenScanner(" = ; : := <> <= < >= > + - * /  [ ] ( ) . ,");
          //List of actual tokens that are needed from the string above.
          Token exspectedtoken1 = new Token("=",TokenType.EQUAL);
          Token exspectedtoken2 = new Token(";",TokenType.SEMICOLON);
          Token exspectedtoken3 = new Token(":",TokenType.COLON);
          Token exspectedtoken4 = new Token(":=",TokenType.ASSIGNOP);
          Token exspectedtoken5 = new Token("<>",TokenType.EQUALCHECK);
          Token exspectedtoken6 = new Token("<=",TokenType.LESS_THAN_EQUAL);
          Token exspectedtoken7 = new Token("<",TokenType.LESS_THAN);
          Token exspectedtoken8 = new Token(">=",TokenType.GREATER_THAN_EQUAL);
          Token exspectedtoken9 = new Token(">",TokenType.GREATER_THAN);
          Token exspectedtoken10 = new Token("+",TokenType.PLUS);
          Token exspectedtoken11 = new Token("-",TokenType.MINUS);
          Token exspectedtoken12 = new Token("*",TokenType.MULTIPLY);
          Token exspectedtoken13 = new Token("/",TokenType.DIVIDE);
          Token exspectedtoken14 = new Token("[",TokenType.LEFT_BRACKET);
          Token exspectedtoken15 = new Token("]",TokenType.RIGHT_BRACKET);
          Token exspectedtoken16 = new Token("(",TokenType.LEFT_PARENTHESIS);
          Token exspectedtoken17 = new Token(")",TokenType.RIGHT_PARENTHESIS);
          Token exspectedtoken18 = new Token(".",TokenType.PERIOD);
          Token exspectedtoken19 = new Token(",",TokenType.COMMA);
          //Scanning each individual token from the string.
          Token actualtoken1 = scanner.nextToken();
          Token actualtoken2 = scanner.nextToken();
          Token actualtoken3 = scanner.nextToken();
          Token actualtoken4 = scanner.nextToken();
          Token actualtoken5 = scanner.nextToken();
          Token actualtoken6 = scanner.nextToken();
          Token actualtoken7 = scanner.nextToken();
          Token actualtoken8 = scanner.nextToken();
          Token actualtoken9 = scanner.nextToken();
          Token actualtoken10 = scanner.nextToken();
          Token actualtoken11 = scanner.nextToken();
          Token actualtoken12 = scanner.nextToken();
          Token actualtoken13 = scanner.nextToken();
          Token actualtoken14 = scanner.nextToken();
          Token actualtoken15 = scanner.nextToken();
          Token actualtoken16 = scanner.nextToken();
          Token actualtoken17 = scanner.nextToken();
          Token actualtoken18 = scanner.nextToken();
          Token actualtoken19 = scanner.nextToken();
          //Asserts used to check if the exspected tokens are the same as the scanned ones.
		  assertEquals(exspectedtoken1.lexeme,actualtoken1.lexeme);
		  assertEquals(exspectedtoken1.type,actualtoken1.type);
		  assertEquals(exspectedtoken2.lexeme,actualtoken2.lexeme);
		  assertEquals(exspectedtoken2.type,actualtoken2.type);
		  assertEquals(exspectedtoken3.lexeme,actualtoken3.lexeme);
		  assertEquals(exspectedtoken3.type,actualtoken3.type);
		  assertEquals(exspectedtoken4.lexeme,actualtoken4.lexeme);
		  assertEquals(exspectedtoken4.type,actualtoken4.type);
		  assertEquals(exspectedtoken5.lexeme,actualtoken5.lexeme);
		  assertEquals(exspectedtoken5.type,actualtoken5.type);
		  assertEquals(exspectedtoken6.lexeme,actualtoken6.lexeme);
		  assertEquals(exspectedtoken6.type,actualtoken6.type);
		  assertEquals(exspectedtoken7.lexeme,actualtoken7.lexeme);
		  assertEquals(exspectedtoken7.type,actualtoken7.type);
		  assertEquals(exspectedtoken8.lexeme,actualtoken8.lexeme);
		  assertEquals(exspectedtoken8.type,actualtoken8.type);
		  assertEquals(exspectedtoken9.lexeme,actualtoken9.lexeme);
		  assertEquals(exspectedtoken9.type,actualtoken9.type);
		  assertEquals(exspectedtoken10.lexeme,actualtoken10.lexeme);
		  assertEquals(exspectedtoken10.type,actualtoken10.type);
		  assertEquals(exspectedtoken11.lexeme,actualtoken11.lexeme);
		  assertEquals(exspectedtoken11.type,actualtoken11.type);
		  assertEquals(exspectedtoken12.lexeme,actualtoken12.lexeme);
		  assertEquals(exspectedtoken12.type,actualtoken12.type);
		  assertEquals(exspectedtoken13.lexeme,actualtoken13.lexeme);
		  assertEquals(exspectedtoken13.type,actualtoken13.type);
		  assertEquals(exspectedtoken14.lexeme,actualtoken14.lexeme);
		  assertEquals(exspectedtoken14.type,actualtoken14.type);
		  assertEquals(exspectedtoken15.lexeme,actualtoken15.lexeme);
		  assertEquals(exspectedtoken15.type,actualtoken15.type);
		  assertEquals(exspectedtoken16.lexeme,actualtoken16.lexeme);
		  assertEquals(exspectedtoken16.type,actualtoken16.type);
		  assertEquals(exspectedtoken17.lexeme,actualtoken17.lexeme);
		  assertEquals(exspectedtoken17.type,actualtoken17.type);
		  assertEquals(exspectedtoken18.lexeme,actualtoken18.lexeme);
		  assertEquals(exspectedtoken18.type,actualtoken18.type);
		  assertEquals(exspectedtoken19.lexeme,actualtoken19.lexeme);
		  assertEquals(exspectedtoken19.type,actualtoken19.type);
	}
	/**
	 * This is for testing numerical values that will be inputed into the code.
	 * It tests simple digits and complex scientific numbers.
	 * @throws IOException
	 */
	@Test
	public void numbertest() throws IOException {
        TokenScanner scanner = null;
          scanner = new TokenScanner("1 2 3 10 27 4212341 3.14 6.9 10.3E3 10.6E+10 10.9E-45");
          //List of actual tokens that are needed from the string above.
          //Test single digit values.
          Token exspectedtoken1 = new Token("1",TokenType.NUMBER);
          Token exspectedtoken2 = new Token("2",TokenType.NUMBER);
          Token exspectedtoken3 = new Token("3",TokenType.NUMBER);
          //Test double or more digit values
          Token exspectedtoken4 = new Token("10",TokenType.NUMBER);
          Token exspectedtoken5 = new Token("27",TokenType.NUMBER);
          Token exspectedtoken6 = new Token("4212341",TokenType.NUMBER);
          //Test fractional values.
          Token exspectedtoken7 = new Token("3.14",TokenType.NUMBER);
          Token exspectedtoken8 = new Token("6.9",TokenType.NUMBER);
          //Test some complex scientific notaion values.
          Token exspectedtoken9 = new Token("10.3E3",TokenType.NUMBER);
          Token exspectedtoken10 = new Token("10.6E+10",TokenType.NUMBER);
          Token exspectedtoken11 = new Token("10.9E-45",TokenType.NUMBER);
          //Scanning each individual token from the string.
          Token actualtoken1 = scanner.nextToken();
          Token actualtoken2 = scanner.nextToken();
          Token actualtoken3 = scanner.nextToken();
          Token actualtoken4 = scanner.nextToken();
          Token actualtoken5 = scanner.nextToken();
          Token actualtoken6 = scanner.nextToken();
          Token actualtoken7 = scanner.nextToken();
          Token actualtoken8 = scanner.nextToken();
          Token actualtoken9 = scanner.nextToken();
          Token actualtoken10 = scanner.nextToken();
          Token actualtoken11 = scanner.nextToken();
          //Asserts used to check if the expected tokens are the same as the scanned ones.
		  assertEquals(exspectedtoken1.lexeme,actualtoken1.lexeme);
		  assertEquals(exspectedtoken1.type,actualtoken1.type);
		  assertEquals(exspectedtoken2.lexeme,actualtoken2.lexeme);
		  assertEquals(exspectedtoken2.type,actualtoken2.type);
		  assertEquals(exspectedtoken3.lexeme,actualtoken3.lexeme);
		  assertEquals(exspectedtoken3.type,actualtoken3.type);
		  assertEquals(exspectedtoken4.lexeme,actualtoken4.lexeme);
		  assertEquals(exspectedtoken4.type,actualtoken4.type);
		  assertEquals(exspectedtoken5.lexeme,actualtoken5.lexeme);
		  assertEquals(exspectedtoken5.type,actualtoken5.type);
		  assertEquals(exspectedtoken6.lexeme,actualtoken6.lexeme);
		  assertEquals(exspectedtoken6.type,actualtoken6.type);
		  assertEquals(exspectedtoken7.lexeme,actualtoken7.lexeme);
		  assertEquals(exspectedtoken7.type,actualtoken7.type);
		  assertEquals(exspectedtoken8.lexeme,actualtoken8.lexeme);
		  assertEquals(exspectedtoken8.type,actualtoken8.type);
		  assertEquals(exspectedtoken9.lexeme,actualtoken9.lexeme);
		  assertEquals(exspectedtoken9.type,actualtoken9.type);
		  assertEquals(exspectedtoken10.lexeme,actualtoken10.lexeme);
		  assertEquals(exspectedtoken10.type,actualtoken10.type);
		  assertEquals(exspectedtoken11.lexeme,actualtoken11.lexeme);
		  assertEquals(exspectedtoken11.type,actualtoken11.type);
	}
}

