/**
 * TokenScanner.java
 * Compilers 2
 * January 27, 2019
 * This is a scanner that uses regular expression to find tokens in a file.
 * @author Nghia Huynh
 * @version 2
 */

/* Declarations */
package scanner;

import java.util.HashMap;


%%
%class  TokenScanner   /* Names the produced java file */
%public					/*Makes the project file public */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%line               /*turn on line counting*/
%column              /*turn on column counting*/

%{
	//Creates a lookup table so our scanner can find key words/characters.
    private HashMap<String,TokenType> lookupTable = new HashMap<String,TokenType>();
	
	/**
	* This code is used to build the lookup table.
	*/
	private void buildTable()
	{
		//When the scanner is created this code bellow adds to the lookupTable.
		//This is the list of key words. 
		lookupTable.put("integer", TokenType.INTEGER);
		lookupTable.put("real", TokenType.REAL);
		lookupTable.put("while", TokenType.WHILE);
		lookupTable.put("do", TokenType.DO);
		lookupTable.put("not", TokenType.NOT);
		lookupTable.put("and", TokenType.AND);
		lookupTable.put("array", TokenType.ARRAY);
		lookupTable.put("begin", TokenType.BEGIN);
		lookupTable.put("else", TokenType.ELSE);
		lookupTable.put("end", TokenType.END);
		lookupTable.put("if", TokenType.IF);
		lookupTable.put("of", TokenType.OF);
		lookupTable.put("or", TokenType.OR);
		lookupTable.put("program", TokenType.PROGRAM);
		lookupTable.put("procedure", TokenType.PROCEDURE);
		lookupTable.put("then", TokenType.THEN);
		lookupTable.put("var", TokenType.VARIABLE);
		lookupTable.put("function",TokenType.FUNCTION);
		lookupTable.put("div",TokenType.DIV);
		lookupTable.put("mod",TokenType.MODULUS);
		lookupTable.put("read",TokenType.READ);
		lookupTable.put("write",TokenType.WRITE);
		lookupTable.put("return",TokenType.RETURN);
		//This is the list of key symbols
		lookupTable.put("=", TokenType.EQUAL);
		lookupTable.put(";", TokenType.SEMICOLON);
		lookupTable.put(":", TokenType.COLON);
		lookupTable.put(":=", TokenType.ASSIGNOP);
		lookupTable.put("<>", TokenType.EQUALCHECK);
		lookupTable.put("<=", TokenType.LESS_THAN_EQUAL);
		lookupTable.put("<", TokenType.LESS_THAN);
		lookupTable.put(">=", TokenType.GREATER_THAN_EQUAL);
		lookupTable.put(">", TokenType.GREATER_THAN);
		lookupTable.put("+", TokenType.PLUS);
		lookupTable.put("-", TokenType.MINUS);
		lookupTable.put("*", TokenType.MULTIPLY);
		lookupTable.put("/", TokenType.DIVIDE);
		lookupTable.put("[", TokenType.LEFT_BRACKET);
		lookupTable.put("]", TokenType.RIGHT_BRACKET);
		lookupTable.put("(", TokenType.LEFT_PARENTHESIS);
		lookupTable.put(")", TokenType.RIGHT_PARENTHESIS);
		lookupTable.put(".", TokenType.PERIOD);
		lookupTable.put(",", TokenType.COMMA);
	}
	
	/**
	* Gets the line number of the most recent lexeme.
	* @return The current line number.
	*/
	public int getLine()
	{ 
	return yyline;
	}
  
	/**
	* Gets the column number of the most recent lexeme.
	* This is the number of chars since the most recent newline char.
	* @return The current column number.
	*/
	public int getColumn()
	{ 
	return yycolumn;
	}
  
  	/**
	*This builds a scanner with a string input.
	*/
     public TokenScanner(String string) {
			buildTable();
    this.zzReader = new java.io.StringReader(string);
  }
  
%}

%init{
	buildTable();
%init}

%eofval{
  return null;
%eofval}
/* Patterns */

other         	  = .
letter            = [A-Za-z]
word         	  = {letter}({letter}|{digits})*
digit		  	  = [0-9]
digits		      =	{digit}{digit}*
optional_fraction = \.{digits}
optional_exponent = [eE]{addop}?{digits}
number			  =	{digits}{optional_fraction}?{optional_exponent}?
whitespace        = [ \n\t\r]
relop		      = <=?|<>?|:?=|>=?|:
addop		      = [+|-]
mulop		      = [*|/]
organizer         = [;|\(|\)|\[|\]|\,|\.]

%%
/* Lexical Rules */

{word}     {
             /** Returns the word that was found. */
			 Token answer = null;
			 //This checks for if the word is in the id table, if not make new ID token.
			 if(lookupTable.get(yytext()) == null)
			 {
				answer = new Token(yytext(), TokenType.ID);
			 }
			 //If it is a keyword return the token with correct token value.
			 else
			 {
				answer = new Token(yytext(), lookupTable.get(yytext()));
			 }
			 return answer;
            }
			
{number}	{
				/**Print a  number that was found */
				return(new Token(yytext(),TokenType.NUMBER));
			}
{addop}		{
				/**Return a addop that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}

{mulop}		{
				/**Return a mulop symbol that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}
{relop}		{
				/**Print a relop symbol that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}
{organizer}	{
				/**Print an organizer symbol that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}
{whitespace}
			{
			/**This is ment to ignore white space and new lines*/
			}
			
{other}    { 
             System.out.println("Illegal char: '" + yytext() + "' found.");
           }
		   