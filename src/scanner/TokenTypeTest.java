/**
 * TokenTypeTest.java
 * Compilers 2
 * January 25, 2019
 * This tests to see if the enums are functioning correctly.
 * @author Nghia Huynh
 * @version 1
 */

package scanner;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *This test it to make sure all of the enums are correct and link to the correct token type.
 *
 */
public class TokenTypeTest {

	@Test
	public void test() {
		
		assertEquals(TokenType.valueOf("ID"), TokenType.ID);
		assertEquals(TokenType.valueOf("NUMBER"), TokenType.NUMBER);
		assertEquals(TokenType.valueOf("INTEGER"), TokenType.INTEGER);
		assertEquals(TokenType.valueOf("REAL"), TokenType.REAL);
		assertEquals(TokenType.valueOf("WHILE"), TokenType.WHILE);
		assertEquals(TokenType.valueOf("DO"), TokenType.DO);
		assertEquals(TokenType.valueOf("NOT"), TokenType.NOT);
		assertEquals(TokenType.valueOf("AND"), TokenType.AND);
		assertEquals(TokenType.valueOf("ARRAY"), TokenType.ARRAY);
		assertEquals(TokenType.valueOf("BEGIN"), TokenType.BEGIN);
		assertEquals(TokenType.valueOf("ELSE"), TokenType.ELSE);
		assertEquals(TokenType.valueOf("IF"), TokenType.IF);
		assertEquals(TokenType.valueOf("OF"), TokenType.OF);
		assertEquals(TokenType.valueOf("OR"), TokenType.OR);
		assertEquals(TokenType.valueOf("PROGRAM"), TokenType.PROGRAM);
		assertEquals(TokenType.valueOf("PROCEDURE"), TokenType.PROCEDURE);
		assertEquals(TokenType.valueOf("THEN"), TokenType.THEN);
		assertEquals(TokenType.valueOf("VARIABLE"), TokenType.VARIABLE);
		assertEquals(TokenType.valueOf("EQUAL"), TokenType.EQUAL);
		assertEquals(TokenType.valueOf("SEMICOLON"), TokenType.SEMICOLON);
		assertEquals(TokenType.valueOf("COLON"), TokenType.COLON);
		assertEquals(TokenType.valueOf("EQUALCHECK"), TokenType.EQUALCHECK);
		assertEquals(TokenType.valueOf("LESS_THAN_EQUAL"), TokenType.LESS_THAN_EQUAL);
		assertEquals(TokenType.valueOf("LESS_THAN"), TokenType.LESS_THAN);
		assertEquals(TokenType.valueOf("GREATER_THAN_EQUAL"), TokenType.GREATER_THAN_EQUAL);
		assertEquals(TokenType.valueOf("GREATER_THAN"), TokenType.GREATER_THAN);
		assertEquals(TokenType.valueOf("PLUS"), TokenType.PLUS);
		assertEquals(TokenType.valueOf("MINUS"), TokenType.MINUS);
		assertEquals(TokenType.valueOf("MULTIPLY"), TokenType.MULTIPLY);
		assertEquals(TokenType.valueOf("DIVIDE"), TokenType.DIVIDE);
		assertEquals(TokenType.valueOf("LEFT_BRACKET"), TokenType.LEFT_BRACKET);
		assertEquals(TokenType.valueOf("RIGHT_BRACKET"), TokenType.RIGHT_BRACKET);
		assertEquals(TokenType.valueOf("LEFT_PARENTHESIS"), TokenType.LEFT_PARENTHESIS);
		assertEquals(TokenType.valueOf("RIGHT_PARENTHESIS"), TokenType.RIGHT_PARENTHESIS);
		assertEquals(TokenType.valueOf("PERIOD"), TokenType.PERIOD);
		assertEquals(TokenType.valueOf("COMMA"), TokenType.COMMA);
		assertEquals(TokenType.valueOf("FUNCTION"), TokenType.FUNCTION);
		assertEquals(TokenType.valueOf("DIV"), TokenType.DIV);
		assertEquals(TokenType.valueOf("MODULUS"), TokenType.MODULUS);		
		assertEquals(TokenType.valueOf("READ"), TokenType.READ);
		assertEquals(TokenType.valueOf("WRITE"), TokenType.WRITE);
		assertEquals(TokenType.valueOf("RETURN"), TokenType.RETURN);
	}

}
