/**
 * TokenTest.java
 * Compilers 2
 * January 25, 2019
 * This tests the creation of a token object given a lexeme and a type.
 * @author Nghia Huynh
 * @version 1
 */

package scanner;

import static org.junit.Assert.*;

import org.junit.Test;

public class TokenTest {
    /**
     * Tests to see if the proper lexeme and token type are being added to a new token.
     */
	
	@Test
	public void test() {
		//Create a new apple token
		Token new_token = new Token("Apples",TokenType.ID);
		//What to exspect from the token
		String expected_lexeme = "Apples";
		TokenType expected_type = TokenType.ID;
		//Pull data from out created token
		String actual_lexeme = new_token.lexeme;
		TokenType actual_type = new_token.type;
		//Check if the data matches to insure that the created token is what it is.
		assertEquals(expected_lexeme, actual_lexeme);
        assertEquals(expected_type, actual_type);
	}

}
