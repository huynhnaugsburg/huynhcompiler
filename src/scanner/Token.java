/**
 * Token.java
 * Compilers 2
 * January 27, 2019
 * This creates a token object given a lexeme and a type.
 * @author Nghia Huynh
 * @version 2
 */

package scanner;

public class Token {
	//The lexeme of the token.
    public String lexeme;
	//The token type.
    public TokenType type;
	//The token constructor.
	public Token( String new_lexeme, TokenType new_type) 
	{
        this.lexeme = new_lexeme;
        this.type = new_type;
    }
}