package analyzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import parser.Parser;
import symboltable.KindType;
import symboltable.SymbolTable;
import syntaxtree.ExpressionNode;
import syntaxtree.ProgramNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;

public class SemanticAnalyzerTest {

	/**
	 * This test is used to see if code can be parsed in and produce both tree before and after analysis
	 */
	@Test
	public void testSemanticAnalyzer() {
		//Test on money.pas and tests inputs for all types of operations.
		String code = "./src/analyzer/money.pas";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			String actualbefore = pn.indentedToString(0);
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String actualafter = pn.indentedToString(0);
			String exspectedbefore = 
					"Program: sample\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: INTEGER : dollars\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Name: REAL : bitcoins\n" + 
					"|-- --- Name: REAL : bucks\n" + 
					"|-- --- Name: REAL : meow\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : bucks\n" + 
					"|-- --- --- Name: null : meow\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : dollars\n" + 
					"|-- --- --- Value: null : 1000000\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 110\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- --- Operation: null : DIVIDE\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 3900.1\n" + 
					"|-- --- If: null\n" + 
					"|-- --- --- Operation: null : LESS_THAN\n" + 
					"|-- --- --- --- --- Value: null : 2\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : meow\n" + 
					"|-- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- Value: null : 10\n" + 
					"|-- --- --- --- --- --- Value: null : 10\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : meow\n" + 
					"|-- --- --- --- Operation: null : MINUS\n" + 
					"|-- --- --- --- --- --- Value: null : 10\n" + 
					"|-- --- --- --- --- --- Value: null : 10\n" + 
					"|-- --- Compound Statement\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 20\n" + 
					"|-- --- While: null\n" + 
					"|-- --- --- Operation: null : GREATER_THAN\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- Value: null : 0\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Read: null : dollars\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Return: null\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- Value: null : 2\n";
			String exspectedafter = 
					"Program: sample\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: INTEGER : dollars\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Name: REAL : bitcoins\n" + 
					"|-- --- Name: REAL : bucks\n" + 
					"|-- --- Name: REAL : meow\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : bucks\n" + 
					"|-- --- --- Name: REAL : meow\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- Value: INTEGER : 1000000\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- Operation: INTEGER : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- --- --- Value: INTEGER : 110\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : bitcoins\n" + 
					"|-- --- --- Operation: REAL : DIVIDE\n" + 
					"|-- --- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- --- --- Value: REAL : 3900.1\n" + 
					"|-- --- If: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : LESS_THAN\n" + 
					"|-- --- --- --- --- Value: INTEGER : 2\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- Name: REAL : meow\n" + 
					"|-- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 10\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 10\n" + 
					"|-- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- Name: REAL : meow\n" + 
					"|-- --- --- --- Operation: INTEGER : MINUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 10\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 10\n" + 
					"|-- --- Compound Statement\n" + 
					"|-- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- --- Operation: INTEGER : MULTIPLY\n" + 
					"|-- --- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 20\n" + 
					"|-- --- While: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : GREATER_THAN\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- Value: INTEGER : 0\n" + 
					"|-- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Read: INTEGER : dollars\n" + 
					"|-- --- Write: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Return: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- Value: INTEGER : 2\n";
			assertEquals(actualbefore,exspectedbefore);
			assertEquals(actualafter,exspectedafter);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize program!");
		}
	}

	/**
	 * This tests a smaller scale version of operations testing. with both a before and after analysis for jsut ints.
	 */
	@Test
	public void testFindNodesandTypeWalk() {
		String code = "1 + 2 + 3 * 4";
		Parser recog = new Parser(code);
		try 
		{
			ExpressionNode node = recog.expression();
			String actualbefore = node.indentedToString(0);
			SemanticAnalyzer analyzer = new SemanticAnalyzer(null,null);
			analyzer.findNodes(node);
			String actualafter = node.indentedToString(0);
			String exspectedbefore = 
					"Operation: null : PLUS\n" + 
					"|-- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- Value: null : 2\n" + 
					"|-- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- Value: null : 3\n" + 
					"|-- --- --- --- Value: null : 4\n";
			String exspectedafter = 
					"Operation: INTEGER : PLUS\n" + 
					"|-- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- Value: INTEGER : 2\n" + 
					"|-- --- Operation: INTEGER : MULTIPLY\n" + 
					"|-- --- --- --- Value: INTEGER : 3\n" + 
					"|-- --- --- --- Value: INTEGER : 4\n";
			assertEquals(actualbefore,exspectedbefore);
			assertEquals(actualafter,exspectedafter);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to analyze nodes!");
		}
	}

	
	/**
	 * Tests to see if the checkValue function is identifying ints and reals correctly.
	 */
	@Test
	public void testCheckValue() {
		try
		{
			SemanticAnalyzer analyzer = new SemanticAnalyzer(null, null);
			ValueNode node1 = new ValueNode("200");
			ValueNode node2 = new ValueNode("2.5");
			assertEquals(DataType.INTEGER,analyzer.checkValue(node1));
			assertEquals(DataType.REAL,analyzer.checkValue(node2));
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to analyze nodes!");
		}
	}

	/**
	 * Checks to see if checkVariable can pull the correct types from the symboltable and return them.
	 */
	@Test
	public void testCheckVariable() {
		try
		{
			SymbolTable table = new SymbolTable();
			table.addSymbol("num1", KindType.VARIABLE, DataType.INTEGER);
			table.addSymbol("num2", KindType.VARIABLE, DataType.REAL);
			SemanticAnalyzer analyzer = new SemanticAnalyzer(null,table);
			VariableNode node1 = new VariableNode("num1");
			VariableNode node2 = new VariableNode("num2");
			assertEquals(DataType.INTEGER,analyzer.checkVariable(node1));
			assertEquals(DataType.REAL,analyzer.checkVariable(node2));
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to analyze nodes!");
		}
	}

	/**
	 * This tests the checkType function and sees if it returns the correct child type from two parents.
	 */
	@Test
	public void testCheckType() {
		try
		{
			SemanticAnalyzer analyzer = new SemanticAnalyzer(null,null);
			assertEquals(DataType.REAL,analyzer.checkType(DataType.REAL, DataType.INTEGER));
			assertEquals(DataType.REAL,analyzer.checkType(DataType.INTEGER, DataType.REAL));
			assertEquals(DataType.INTEGER,analyzer.checkType(DataType.INTEGER, DataType.INTEGER));
			assertEquals(null,analyzer.checkType(null, null));
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to analyze nodes!");
		}
	}

	/**
	 * Tests if ints are being properly recognized.
	 */
	@Test
	public void testIsInteger() {
		try
		{
			SemanticAnalyzer analyzer = new SemanticAnalyzer(null,null);
			assertEquals(true,analyzer.isInteger("5"));
			assertEquals(false,analyzer.isInteger("5.5"));
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to analyze nodes!");
		}
	}

}
