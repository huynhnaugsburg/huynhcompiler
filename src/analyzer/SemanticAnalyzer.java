package analyzer;

import java.util.ArrayList;

import scanner.TokenType;
import symboltable.*;
import syntaxtree.ArrayNode;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.ReadStatementNode;
import syntaxtree.ReturnStatementNode;
import syntaxtree.StatementNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileDoStatementNode;
import syntaxtree.WriteStatementNode;

/**
 * SemanticAnalyzer.java
 * Compilers 2
 * April 9, 2019
 * This code is used to analyze the indeted to string and check for the types of expressions and assignements.
 * @author Nghia Huynh
 * @version 2
 */
public class SemanticAnalyzer {

	private SymbolTable table;
	private ProgramNode node;
	
    /**
     * This is the constructor for the semantic analyzer.
     * @param node The starting node of the syntax tree
     * @param table The symbol table produced during parsing
     */
    public SemanticAnalyzer(ProgramNode node, SymbolTable table) {
    	this.node = node;
    	this.table = table;
    }

	/**
	 * This analyzes the internal program node and the symboltable given by the constructor and checks for 
	 * if the data types are correct for each statement.
	 * @return A boolean value if the checks performed did not find an error, false otherwise.
	 */
	public boolean analyze()
    {
    	boolean answer = true;
    	//These two lines creats a list statementes from the given program node.
    	answer = checkCompoundStatement(this.node.getMain());
    	
    	return answer;
    }
	
	/**
	 * This method takes in a given statement node and determines its instace object and calls the function 
	 * for that specific node.
	 * @param statement This is the node that will be checked for what kind of statement it is
	 * @return returns a boolean if the checks performed have passed, false if any failed.
	 */
	public boolean checkStatementSwitch(StatementNode statement)
	{
		boolean answer = true;
		if(statement instanceof AssignmentStatementNode)
		{
			answer = checkAssignments((AssignmentStatementNode) statement);
		}
		else if(statement instanceof CompoundStatementNode)
		{
			answer = checkCompoundStatement((CompoundStatementNode) statement);
		}
		else if(statement instanceof IfStatementNode)
		{
			answer = checkIfStatement((IfStatementNode) statement);
		}
		else if(statement instanceof WhileDoStatementNode)
		{
			answer = checkWhileDoStatement((WhileDoStatementNode) statement);
		}
		else if(statement instanceof ReadStatementNode)
		{
			answer = checkReadStatement((ReadStatementNode) statement);
		}
		else if(statement instanceof WriteStatementNode)
		{
			answer = checkWriteStatement((WriteStatementNode) statement);
		}
		else if(statement instanceof ReturnStatementNode)
		{
			answer = checkReturnStatement((ReturnStatementNode) statement);
		}
		return answer;
	}
	
	/**
	 * This checks if an assignment is being put in the correct variable type.
	 * @param node The assignment node that is going to be checked
	 * @return Boolean for if the check passed.
	 */
	public boolean checkAssignments(AssignmentStatementNode node)
	{
		boolean answer =  true;
		//Sets empty data types for later checking.
		DataType variable = null;
		DataType expression = null;
		//These two variables are for storing data of the current assignment node.
		VariableNode var = node.getLvalue();
		ExpressionNode exp = node.getExpression();
		//This checks if the variable exists within the table.
		if(table.isVariable(var.getName()))
		{
			//This gets the data type for the variable and then assigns it.
			if(var instanceof ArrayNode)
			{
				variable = table.isType(var.getName());
				findNodes(node.getLvalue());
				node.getLvalue().setDataType(variable);
			}
			else
			{
				variable = table.isType(var.getName());
				node.getLvalue().setDataType(variable);
			}
		}
		else
		{
			//Tells user that the variable has not been declared and returns false.
			System.out.println("Warning: "+  var + " has not been declared!");
			answer = false;
		}
			//This gets the data type of the expression part of the assignment.
		expression = findNodes(exp);
		//Based on what the data types of variable and expression it sets a data type for the assignment.
		//If same set variable as type
		if(variable == expression)
		{
			node.setDataType(variable);
		}
			//If var is real but expression is int the assignment is real.
		else if (variable == DataType.REAL && expression == DataType.INTEGER)
		{
			node.setDataType(DataType.REAL);
		}
		//Else if it sees anything else then there is a type mismatch
		else
		{
			System.out.println("Warning: Assignment mismatch!");
			answer = false;
		}
		return answer;
	}
	
	/**
	 * Checks a list of compound statements to see if they are all of the correct type
	 * @param node A compoundstatement node that is going to be checked.
	 * @return a boolean value if no check failed, else false.
	 */
	public boolean checkCompoundStatement(CompoundStatementNode node)
	{
		boolean answer = true;
		ArrayList<StatementNode> statements = node.getStatements();
		//Loops though the gathered statements and runs the check statement on each.
    	for(StatementNode statement: statements)
    	{
    		answer = answerSwitch(answer, checkStatementSwitch(statement));
    	}
		return answer;
	}
	
	/**
	 * This checks an if statement node and sees if the types for each statement is correct
	 * @param node The if statement node that is being checked
	 * @return The boolean value if the check succeeded or not.
	 */
	public boolean checkIfStatement(IfStatementNode node)
	{
		boolean answer = true;
		DataType var = findNodes(node.getTest());
		//Checks if the test node is of type operation.
		if(node.getTest() instanceof OperationNode)
		{
			TokenType token = ((OperationNode) node.getTest()).getOperation();
			//The if statement needs a conditional somewhere inside of its operations so that must be checked.
	    	if(token == TokenType.EQUAL
	    			||	token == TokenType.EQUALCHECK
	    	    	||	token == TokenType.LESS_THAN
	    	    	||	token == TokenType.LESS_THAN_EQUAL
	    	    	||	token == TokenType.GREATER_THAN_EQUAL
	    	    	||	token == TokenType.GREATER_THAN
	    	    	)
	    	    	{
	    	    		answer = true;
	    	    	}
	    	    	else
	    	    	{
	    	    		System.out.println("Warning: If Then Else statement is missing a relop!");
	    	    		answer = answerSwitch(answer, false);
	    	    	}
		}
		else
		{
			answer = answerSwitch(answer, false);
		}
		//If the check from earlier for the type failed then the code returns false and the type mismatch.
		if (var == null)
		{
			System.out.println("Warning: Expression type mismatch!");
			answer = answerSwitch(answer, false);
		}
		node.setDataType(var);
		answer = answerSwitch(answer,checkStatementSwitch(node.getThenStatement()));
		answer = answerSwitch(answer,checkStatementSwitch(node.getElseStatement()));
		return answer;
	}
	
	/**
	 * The code for a while do statement where it checks if the there is a conditional and then checks
	 * if the looped expression has the correct data  assignments.
	 * @param node The while do statement node that is being checked.
	 * @return This returns the boolean if the check succeeded.
	 */
	public boolean checkWhileDoStatement(WhileDoStatementNode node)
	{
		boolean answer = true;
		DataType var = findNodes(node.getExpression());
		if(node.getExpression() instanceof OperationNode)
		{
			TokenType token = ((OperationNode) node.getExpression()).getOperation();
			//The while statement needs a conditional somewhere inside of its operations so that must be checked.
	    	if(token == TokenType.EQUAL
	    			||	token == TokenType.EQUALCHECK
	    	    	||	token == TokenType.LESS_THAN
	    	    	||	token == TokenType.LESS_THAN_EQUAL
	    	    	||	token == TokenType.GREATER_THAN_EQUAL
	    	    	||	token == TokenType.GREATER_THAN
	    	    	)
	    	    	{
	    	    		answer = true;
	    	    	}
	    	    	else
	    	    	{
	    	    		System.out.println("Warning: While statement is missing a relop!");
	    	    		answer = answerSwitch(answer,false);
	    	    	}
		}
		//If the check from earlier for the type failed then the code returns false and the type mismatch.
		if (var == null)
		{
			System.out.println("Warning: Expression type mismatch!");
			answer = answerSwitch(answer,false);
		}
		node.setDataType(var);
		answer = answerSwitch(answer,checkStatementSwitch(node.getStatement()));
		return answer;
	}
	
	/**
	 * This checks the typing of the readstatement node and sees if it is the correct types
	 * @param node The read statement node that will be checked.
	 * @return The boolean that will be returned if the check succeeded or failed.
	 */
	public boolean checkReadStatement(ReadStatementNode node)
	{
		boolean answer = true;
		DataType var = this.table.isType(node.getName());
		//This checks if the variable being read to has been declared.
		if (var == null)
		{
			System.out.println("Warning: "+ node.getName() + " has not been declared!");
			answer = false;
		}
		//Set data type of the node.
		node.setDataType(var);
		return answer;
	}
	
	/**
	 * Write statement node is used to check for when something is being solved and written to the console.
	 * @param node The writestatement node to be looked at.
	 * @return The boolean for when the checks succeed or failed.
	 */
	public boolean checkWriteStatement(WriteStatementNode node)
	{
		boolean answer = true;

		DataType var = findNodes(node.getExpression());
		//This checks if the expression being written has the correct types.
		if (var == null)
		{
			System.out.println("Warning: Expression type mismatch!");
			answer = false;
		}
		//Set data type of the node.
		node.setDataType(var);
		return answer;
	}
	
	/**
	 * This checks the return statement and sees if the expression is of the correct types.
	 * @param node The node to be checked by the method
	 * @return The boolean value for when the checks succeed or failed.
	 */
	public boolean checkReturnStatement(ReturnStatementNode node)
	{
		boolean answer = true;
		DataType var = findNodes(node.getExpression());
		if (var == null)
		{
			System.out.println("Warning: Expression type mismatch!");
			answer = false;
		}
		node.setDataType(var);
		return answer;	
	}
	
	

	/**
	 * Takes in a given expression node and returns is type
	 * @param input a given expression node
	 * @return a DataType enum value
	 */
	public DataType findNodes(ExpressionNode input)
    {
		DataType type = null;
    	if(input instanceof OperationNode)
    	{
    		type = typeWalk((OperationNode)input);
    		input.setDataType(type);
    	}
    	if(input instanceof ValueNode)
    	{
    		type = checkValue((ValueNode) input);
    	}
    	if(input instanceof ArrayNode)
    	{
    		type = checkArray((ArrayNode) input);
    	}
    	else if(input instanceof VariableNode)
    	{
    		type = checkVariable((VariableNode) input);
    	}
    	return type;
    }
    
    /**
     * This will recursively go down a group of operations node and assigns the types
     *  based on given values and variables
     * @param node a given operation node
     * @return the DataType enum  value
     */
    public DataType typeWalk(OperationNode node)
    {
    	//Sets blank datatype for children of operation node.
    	DataType left = null;
    	DataType right = null;
    	//If a child is operation node call typewalk on them and set the returned type value to that node.
    	if (node.getLeft() instanceof OperationNode)
    	{
    		left = typeWalk((OperationNode)node.getLeft());
    		node.getLeft().setDataType(left);
    	}
    	if(node.getRight() instanceof OperationNode)
    	{
    		right = typeWalk((OperationNode)node.getRight());
    		node.getRight().setDataType(right);
    	}
    	//If child node is a value node check the value and see if its and int or real and return that type.
    	if (node.getLeft() instanceof ValueNode)
    	{
    		left = checkValue((ValueNode) node.getLeft());
    	}
    	if (node.getRight() instanceof ValueNode)
    	{
    		right = checkValue((ValueNode) node.getRight());
    	}
    	
    	//Check if array node and return its type based on the node.
    	if (node.getLeft() instanceof ArrayNode)
    	{
    		left = checkArray((ArrayNode) node.getLeft());
    	}
    	else if (node.getLeft() instanceof VariableNode)
    	{
    		left = checkVariable((VariableNode) node.getLeft());
    	}
    	//-----------------------------------------------------
    	if (node.getRight() instanceof ArrayNode)
    	{
    		right = checkArray((ArrayNode) node.getRight());
    	}
    	else if (node.getRight() instanceof VariableNode)
    	{
    		right = checkVariable((VariableNode) node.getRight());
    	}
		return checkType(left, right);
    }
    
    /**
     * Takes in a value node and gets the value and checks to see if its an int or real
     * @param node a given value node
     * @return the datatype of a value node.
     */
    public DataType checkValue(ValueNode node)
    {
    	DataType type = null;
    	
		if(isInteger(node.getAttribute()))
		 	{
			 	type = DataType.INTEGER;
		 	}
		 	else
		 	{
		 		type = DataType.REAL;
		 	}
		node.setDataType(type);
    	
		return type;
    }
    
    /**
     * Takes in a variable node and gets the type from the table.
     * Has warning for is the varible not in table/declared
     * @param node a given value node
     * @return the datatype of a variable node.
     */
    public DataType checkVariable(VariableNode node)
    {
    	DataType type = table.isType(node.getName());
		node.setDataType(type);
		if(type == null)
		{
			System.out.println("Warning: "+  node.getName() + " has not been declared!" );
			return null;
		}
    	return type;
    }
    
    public DataType checkArray(ArrayNode node)
    {
    	DataType type = table.isType(node.getName());
		node.setDataType(type);
		if(type == null)
		{
			System.out.println("Warning: "+  node.getName() + " has not been declared!" );
			return null;
		}
		DataType expressiontype = findNodes(node.getExpression());
		node.getExpression().setDataType(expressiontype);
		if(expressiontype == null)
		{
			System.out.println("Warning: "+  node.getName() + " has a bad expression!" );
			return null;
		}
		else if(expressiontype == DataType.REAL)
		{
			System.out.println("Warning: "+  node.getName() + " has real value for an index!" );
			return null;
		}
		return type;
    }
    
    /**
     * Takes in two data types and returns what the child datatype should be.
     * @param left left node Datatype
     * @param right right node datatype
     * @return Final dataype of the two children
     */
    public DataType checkType(DataType left, DataType right)
    {
    	DataType type = null;
    	//If either one is real return real
    	if(left == DataType.REAL || right == DataType.REAL)
    	{
    		type = DataType.REAL;
    	}
    	//If both are ints return ints
    	else if(left == DataType.INTEGER  &&  right == DataType.INTEGER)
    	{
    		type = DataType.INTEGER;
    	}
    	else
    	{
    		type = null;
    	}
    	return type;
    }
    
    /**
     * This takes in a string and uses regular expression to see if the string is a int.
     * @param input the string to see if its an int
     * @return returns a boolean true if it is an int and false otherwise.
     */
    public boolean isInteger(String input)
    {
    	if(input.matches("-?(0|[1-9]\\d*)"))
    	{
    		return true;
    	}
    	return false;
    }
    
    /**
     * This is a switch used for methods that have multiple chances to reset the false value of an answer
     * @param answer The original answer value
     * @param swap The new answer value
     * @return The answer after the check;
     */
    public boolean answerSwitch(boolean answer, boolean swap)
    {
    	if (answer == false)
    	{
    		return false;
    	}
    	return swap;
    }
    
}
