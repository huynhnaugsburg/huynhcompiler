/**
 * DataTypeTest.java
 * Compilers 2
 * April 9, 2019
 * This tests to see if the enums are functioning correctly.
 * @author Nghia Huynh
 * @version 1
 */
package analyzer;

import static org.junit.Assert.*;

import org.junit.Test;

import symboltable.KindType;

public class DataTypeTest {
	/**
	 *This test it to make sure all of the enums are correct and link to the correct kind type.
	 *
	 */
	@Test
	public void test() {
		assertEquals(DataType.valueOf("INTEGER"), DataType.INTEGER);
		assertEquals(DataType.valueOf("REAL"), DataType.REAL);
	}

}
