/**
 * CompilerMainTest.java
 * Compilers 2
 * March 2, 2019
 * This code tests the main function and jsut makes sure the file can be created and written to.
 * @author Nghia Huynh
 * @version 1
 */
package compiler;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.junit.Test;

import parser.Recognizer;

public class CompilerMainTest {

	@Test
	public void testMain() {
		Recognizer recog = new Recognizer("./src/compiler/testgoodcode.pas");
		//This try catch is used to check if the inputed code is correct and if not it exits the code.
		try
		{
			recog.program();
			//System.out.println("Success! File has been recognized!");
			//Prints table to console for user.
			//System.out.println(recog.toString());
		}
		catch(RuntimeException e)
		{
			//Tells user the failure and exits the program.
			System.out.println("Failure! File has not been recognized!");
			fail("Failed to recognize file!");
			System.exit(0);
		}
		
		//Creates a null fileoutputsream and a bufferedwriter.
		FileOutputStream fileOutput = null;
		BufferedWriter fileWriter = null;
		//This try catch is used to see if we can create the file and if we can open a writing stream to it.
		//Else the code will close and will not run.
		try
		{
			fileOutput = new FileOutputStream("SymbolTable.txt");
			fileWriter = new BufferedWriter(new OutputStreamWriter(fileOutput));
		}
		catch (FileNotFoundException e)
		{
			/*
			 * This exception closes the code if it could not write to a file.
			 */
			System.out.println("Could not create SymbolTable.txt! Closing program!");
			fail("Failed to create the file and the write stream!");
			System.exit(0);
		}
		//This try catch is used to tak the toString method from the recognizer and put that 
		//information into the new SynbolTable.txt file.
		try 
		{
			//System.out.println("Writing Symbol Table to file!");
			fileWriter.write(recog.toString());
			fileWriter.flush();
			fileWriter.close();
			//System.out.println("Done!");
			//This lets the user know where the file is now located at.
			String path =  new String(new File("SymbolTable.txt").getAbsolutePath());
			//System.out.println("Here is where the file is: " + path);
		}
		//This ctach is for when the file could not be written to.
		catch (IOException e)
		{
			System.out.println("Could not write to file! Closing program!");
			fail("Failed to write to the file!");
			System.exit(0);
		}
		boolean exists = true;
		File file = new File("SymbolTable.txt");
		assertEquals(file.exists(), exists);
	}

}
