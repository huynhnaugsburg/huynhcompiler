/**
 * CompilerMain.java
 * Compilers 2
 * March 2, 2019
 * This is a main used to run the recognizer code on a given pascal file..
 * @author Nghia Huynh
 * @version 1
 */

package compiler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import analyzer.SemanticAnalyzer;
import codegenerator.CodeGenerator;
import parser.*;
import syntaxtree.ProgramNode;

public class CompilerMain {

	public static void main(String[] args) {
	//This line is used to create a new recognizer object and takes in a given .pas file to be read.
	Parser recog = null;
	String syntaxtree = null;
	ProgramNode pn = null;
	boolean buildcode = false;
	//This try catch is used to check if the inputed code is correct and if not it exits the code.
	try
	{
		recog = new Parser(args[0]);
		pn = recog.program();
		//Creates an analyzer using the new program node and symbol table from recog
		SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
		buildcode = analyzer.analyze();
		syntaxtree = pn.indentedToString(0);
		
		System.out.println("Success! File has been recognized!");
		//Prints table to console for user.
		System.out.println(recog.toString());
		System.out.println(syntaxtree);
	}
	catch(RuntimeException e)
	{
		//Tells user the failure and exits the program.
		System.out.println(e.getMessage());
		System.out.println("Failure! File has not been recognized!");
		System.exit(0);
	}
	writeFile(recog.toString(),args[0],"table");
	writeFile(syntaxtree,args[0],"syntax");
	if(buildcode)
	{
		CodeGenerator generator =  new CodeGenerator();
		String pascalcode = generator.writeCodeForProgram(pn);
		writeFile(pascalcode,args[0],"asm");
	}
	
	}
	
	public static void writeFile(String stuffToWrite, String fileLocation, String fileType)
	{
		
		//Creates a null fileoutputsream and a bufferedwriter.
		FileOutputStream fileOutput = null;
		BufferedWriter fileWriter = null;
		//This code below creates a file object based on the given arg then grabs its name
		//to be reused in the new .table file.
		File file = new File(fileLocation);
		String newfile = file.getName().substring(0, file.getName().lastIndexOf('.'));
		newfile = newfile + "." + fileType;
		//This try catch is used to see if we can create the file and if we can open a writing stream to it.
		//Else the code will close and will not run.
		try
		{
			fileOutput = new FileOutputStream(newfile);
			fileWriter = new BufferedWriter(new OutputStreamWriter(fileOutput));
		}
		catch (FileNotFoundException e)
		{
			/*
			 * This exception closes the code if it could not write to a file.
			 */
			System.out.println("Could not create File! Closing program!");
			System.exit(0);
		}
		//This try catch is used to take the toString method from the recognizer and put that 
		//information into the new SynbolTable.txt file.
		try 
		{
			System.out.println("Writing to file!");
			fileWriter.write(stuffToWrite);
			fileWriter.flush();
			fileWriter.close();
			System.out.println("Done!");
			//This lets the user know where the file is now located at.
			String path =  new String(new File(newfile).getAbsolutePath());
			System.out.println("Here is where the file is: " + path);
		}
		//This ctach is for when the file could not be written to.
		catch (IOException e)
		{
			System.out.println("Could not write to file! Closing program!");
			System.exit(0);
		}

	}
}


