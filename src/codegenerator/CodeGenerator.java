package codegenerator;

import java.util.ArrayList;

import analyzer.DataType;
import scanner.TokenType;
import syntaxtree.*;

public class CodeGenerator {

	/**
	 * This is the current register that the class is looking at.
	 */
	private int currentTRegister = 0;
	private int currentFRegister = 2;
	/**
	 * The program node that will be walked by the methods to find the correct nodes.
	 */
	private ProgramNode root;
	
	/**
	 * This vairbale is used to keep track of the number of if and while statements that have been nested
	 */
	private int operationNumber = 0;
	
	/**
	 * This is the main method used to build the machine code string.
	 * @param root This is the root of a program node.
	 * @return This returns the string builder as a string.
	 */
	public String writeCodeForProgram(ProgramNode root)
	{
		this.root = root;
		StringBuilder code = new StringBuilder();
		code.append( ".data\n");
		code = writeDeclarations(code);
		code.append( ".text\n");
		code.append( "main:\n");
		code = writeCompoundStatement(root.getMain(),code);
        code.append( "li  $v0,   10\n");
        code.append( "syscall\n");
		return code.toString();
	}
	
	/**
	 * This takes in a declarations node and writes all the variables to the mips program.
	 * @param code The code to be modified.
	 * @return the newly modifed stringbuilder with the new varaibles.
	 */
	public StringBuilder writeDeclarations(StringBuilder code)
	{
		ArrayList<VariableNode> vars = root.getVariables().getVariableList();
		for(VariableNode variable: vars)
		{
			if (variable.getDataType() == DataType.INTEGER)
			{
				if(variable instanceof ArrayNode)
				{
					code.append( variable.getName() + ":   .word   ");
					for(int i = 1; i < ((ArrayNode) variable).getSize(); i++)
					{
						code.append("0, ");
					}
					code.append("0\n");
				}
				else
				{
					code.append( variable.getName() + ":   .word   0\n");
				}
			}
			else if(variable.getDataType() == DataType.REAL)
			{
				if(variable instanceof ArrayNode)
				{
					code.append( variable.getName() + ":   .double   ");
					for(int i = 1; i < ((ArrayNode) variable).getSize(); i++)
					{
						code.append("0.0, ");
					}
					code.append("0.0\n");
				}
				else
				{
					code.append( variable.getName() + ":   .double   0.0\n");
				}
			}
		}
		code.append("\n\n");
		return code;
	}
	
	/**
	 * 
	 * @param code takes in stringbuilder that has the code for machine language code.
	 * @return The modified string after the compound statements have been added.
	 */
	
	public StringBuilder statementSwitch(StatementNode statement, StringBuilder code)
	{
		if(statement instanceof AssignmentStatementNode)
		{
			code = writeAssignments((AssignmentStatementNode) statement, code);
		}
		else if(statement instanceof CompoundStatementNode)
		{
			code = writeCompoundStatement((CompoundStatementNode) statement, code);
		}
		else if(statement instanceof IfStatementNode)
		{
			code = writeIfStatement((IfStatementNode) statement, code);	
		}
		else if(statement instanceof WhileDoStatementNode)
		{
			code = writeWhileDoStatement((WhileDoStatementNode) statement, code);
		}
		else if(statement instanceof ReadStatementNode)
		{
			code = writeReadStatement((ReadStatementNode) statement, code);
		}
		else if(statement instanceof WriteStatementNode)
		{
			code = writeWriteStatement((WriteStatementNode) statement, code);
		}
		else if(statement instanceof ReturnStatementNode)
		{
			code = writeReturnStatement((ReturnStatementNode) statement, code);
		}
		return code;
	}
	
	
	/**
	 * Takes in a stringbuilder and adds a store word assignment from a register to a variable.
	 * @param code Strinbuilder to be modified.
	 * @param reg The register that holds the answer for the variable
	 * @param variable The variable that will be written into
	 * @return returns the modified string builder.
	 */
	public StringBuilder writeAssignments(AssignmentStatementNode node, StringBuilder code)
	{
		if(node.getDataType() == DataType.REAL)
		{
			String expcode = null;
			int tempReg = this.currentFRegister;
			ExpressionNode exp = node.getExpression();
			expcode = writeExpression(exp,"$f0", null, node.getDataType());
			this.currentFRegister = tempReg;
			code.append(expcode);
			//This part then takes the value from the expression output and puts it into the assigned variable
			String variable = node.getLvalue().getName();
			if(node.getLvalue() instanceof ArrayNode)
			{
	        	String adressregister =  "$f" + (currentTRegister+2);
	        	String indexregister = "$f" + (currentTRegister+2);
	        	code.append("ldc1     " + adressregister + ",   "  + variable + "\n");
	        	code.append(writeExpression(((ArrayNode) node.getLvalue()).getExpression(), indexregister , null, node.getLvalue().getDataType()));
	        	code.append("li.d " + indexregister + ", " + ((ArrayNode) node.getLvalue()).getOffSet() + "\n");
	        	code.append("sll " + indexregister + ", " + indexregister + ", " + "2\n");
	        	code.append("add.d " + adressregister + ", " + adressregister + ", " + indexregister + "\n");
	        	code.append("sw  $f0, 0(" + adressregister + ")\n\n");
	    		return(code);
			}
			else
			{
				code.append( "sdc1     " + "$f0,   " +  variable + "\n\n");
			}
			return code;
		}
		else
		{
			//This part grabs an expression and writes it to the stringbuilder.
			String expcode = null;
			int tempReg = this.currentTRegister;
			ExpressionNode exp = node.getExpression();
			expcode = writeExpression(exp,"$s0", null, node.getDataType());
			this.currentTRegister = tempReg;
			code.append(expcode);
			//This part then takes the value from the expression output and puts it into the assigned variable
			String variable = node.getLvalue().getName();
			if(node.getLvalue() instanceof ArrayNode)
			{
	        	String adressregister =  "$t" + currentTRegister++;
	        	String indexregister = "$t" + currentTRegister++;
	        	code.append("la     " + adressregister + ",   "  + variable + "\n");
	        	code.append(writeExpression(((ArrayNode) node.getLvalue()).getExpression(), indexregister , null, node.getLvalue().getDataType()));
	        	code.append("addi " + indexregister + ", " + indexregister + ", " + ((ArrayNode) node.getLvalue()).getOffSet() + "\n");
	        	code.append("sll " + indexregister + ", " + indexregister + ", " + "2\n");
	        	code.append("add " + adressregister + ", " + adressregister + ", " + indexregister + "\n");
	        	code.append("sw  $s0, 0(" + adressregister + ")\n\n");
	    		return(code);
			}
			else
			{
				code.append( "sw     " + "$s0,   " +  variable + "\n\n");
			}
			return code;
		}
	}
	
	public StringBuilder writeCompoundStatement(CompoundStatementNode node, StringBuilder code)
	{
		//This gets all the statements for this node and finds the ones that are assignment nodes.
		ArrayList<StatementNode> statements = node.getStatements();
    	for(StatementNode statement: statements)
    	{
    		//This initialize is in case for when the code might use more than 1 result reg for now its set at 1.
    		code = statementSwitch(statement, code);
    	}
		return code;
	}
	
	/**
	 * This writes an if statement to the given string builder based on a given node
	 * @param node The ifstatementnode used to generate the code.
	 * @param code The code that will be modifified.
	 * @return The modieid code stringbuilder
	 */
	public StringBuilder writeIfStatement(IfStatementNode node, StringBuilder code)
	{
		this.operationNumber++;
		int operationvalue = this.operationNumber; 
		code.append("If"+ operationvalue +":\n");
		code.append(writeExpression(node.getTest(), "$s0", "Then" + operationvalue, node.getDataType()));
		code.append("\nElse" + operationvalue + ":\n");
		code = statementSwitch(node.getElseStatement(), code);
		code.append("j	Endif"+ operationvalue +"\n\n");
		code.append("Then" + operationvalue + ":\n");
		code = statementSwitch(node.getThenStatement(), code);
		code.append("Endif" + operationvalue + ":\n\n");
		return code;
	}
	
	/**
	 * This writes an WhileDoStatementNode to the given string builder based on a given node
	 * @param node The WhileDoStatementNode used to generate the code.
	 * @param code The code that will be modifified.
	 * @return The modieid code stringbuilder
	 */
	public StringBuilder writeWhileDoStatement(WhileDoStatementNode node, StringBuilder code)
	{
		this.operationNumber++;
		int operationvalue = this.operationNumber; 
		code.append("While" + operationvalue + ":\n");
		code.append(writeExpression(node.getExpression(), "$s0", "Do" + operationvalue, node.getDataType()));
		code.append("\nj	EndWhileDo" + operationvalue + "\n");
		code.append("\nDo" + operationvalue + ":\n");
		code = statementSwitch(node.getStatement(),code);
		code.append("j	While" + operationvalue +"\n");
		code.append("\nEndWhileDo" + operationvalue + ":\n\n");
		return code;
	}
	
	/**
	 * This writes an ReadStatementNode to the given string builder based on a given node
	 * @param node The ReadStatementNode  used to generate the code.
	 * @param code The code that will be modifified.
	 * @return The modieid code stringbuilder
	 */
	public StringBuilder writeReadStatement(ReadStatementNode node, StringBuilder code)
	{
		if(node.getDataType() == DataType.REAL)
		{
			code.append("li $v0, 7\n");
	        code.append("syscall\n");
			code.append("sdc1 $f0, " + node.getName() + "\n\n");
		}
		else
		{
			code.append("li $v0, 5\n");
	        code.append("syscall\n");
			code.append("sw $v0, " + node.getName() + "\n\n");
		}
		return code;
	}
	
	/**
	 * This writes an WriteStatementNode to the given string builder based on a given node
	 * @param node The WriteStatementNode used to generate the code.
	 * @param code The code that will be modifified.
	 * @return The modified code stringbuilder
	 */
	public StringBuilder writeWriteStatement(WriteStatementNode node, StringBuilder code)
	{
		if(node.getDataType() == DataType.REAL)
		{
			code.append(writeExpression(node.getExpression(), "$f12", null, node.getDataType()));
	    	code.append("li $v0, 3\n");
	    	code.append("syscall\n\n");
		}
		else
		{
			code.append(writeExpression(node.getExpression(), "$s0", null, node.getDataType()));
	    	code.append("li $v0, 1\n");
	    	code.append("add $a0, $s0, $zero\n");
	    	code.append("syscall\n\n");
		}
    	code.append("addi $a0, $0, 0xA\n");
        code.append("addi $v0, $0, 0xB\n");
        code.append("syscall\n\n");
		return code;
	}
	
	/**
	 * This writes an ReturnStatementNod to the given string builder based on a given node
	 * @param node The ReturnStatementNod used to generate the code.
	 * @param code The code that will be modifified.
	 * @return The modifed code stringbuilder
	 */
	public StringBuilder writeReturnStatement(ReturnStatementNode node, StringBuilder code)
	{
		//WAS NOT SURE WHAT THIS FUNCTION ACTUALLY DID SO THIS IS ALL IT DOES.
		code.append(writeExpression(node.getExpression(), "$v0", null, node.getDataType()));
		code.append("\n\n");
		return code;	
	}
	
    /**
     * This is the start of an expression node and ony works on operation, value, and variables nodes
     * @param node This expression node to be travled down
     * @param reg The register where the node will start writing values to
     * @return returns the string for the operations preformed within this node as machine code.
     */
    public String writeExpression(ExpressionNode node, String reg, String jump, DataType parentNodeType) {
        String nodeCode = "";
        if( node instanceof OperationNode) {
            nodeCode = writeOperation( (OperationNode)node, reg, jump, parentNodeType);
        }
        else if( node instanceof ValueNode) {
            nodeCode = writeValue( (ValueNode)node, reg, parentNodeType);
        }
        else if(node instanceof ArrayNode)
        {
        	nodeCode = writeArray((ArrayNode) node,reg, parentNodeType);
        }
        else if(node instanceof VariableNode)
        {
        	nodeCode = writeVariable((VariableNode) node,reg, parentNodeType);
        }
        return nodeCode;
    }
    
    /**
     * This takes in an operations node and travels along its tree to find node to write to the machine code.
     * @param opNode The original operations node that forms the root
     * @param resultRegister The result register of where the answer from the node should be put
     * @return returns a the modified code string.
     */
    public String writeOperation(OperationNode opNode, String resultRegister, String jump, DataType parentNodeType)
    {
    	
        String code = "";
        if (parentNodeType == DataType.REAL)
        { 
        	//Left child work
            ExpressionNode left = opNode.getLeft();
            currentFRegister = currentFRegister + 2;
            String leftRegister = "$f" + currentFRegister;
            code = writeExpression( left, leftRegister, jump, parentNodeType);
            //Right child work
            ExpressionNode right = opNode.getRight();
            currentFRegister = currentFRegister + 2;
            String rightRegister = "$f" + currentFRegister;
            code += writeExpression( right, rightRegister, jump, parentNodeType);
            
            TokenType kindOfOp = opNode.getOperation();
            //This is for when its a plus operation
            if( kindOfOp == TokenType.PLUS)
            {
                code += "add.d    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
          //This is for when its a minus operation
            if( kindOfOp == TokenType.MINUS)
            {
                code += "sub.d    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
          //This is for when its a multiply operation
            if( kindOfOp == TokenType.MULTIPLY)
            {
                code += "mul.d    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
          //This is for when its a divide operation**NOTE THIS WILL RETURN QUOTIENT FOR NOW need work to return remainder**
            if( kindOfOp == TokenType.DIVIDE)
            {
                code += "div.d    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
            //RELOP WRITTEN CODE
            if (kindOfOp == TokenType.EQUAL || kindOfOp == TokenType.EQUALCHECK)
            {
            	code += "c.eq.d   " + leftRegister + ",   " + rightRegister + "\n";
            	code += "bc1t " + jump + "\n";
            }
            if (kindOfOp == TokenType.LESS_THAN)
            {
            	code += "c.lt.d   " + leftRegister + ",   " + rightRegister + "\n";
            	code += "bc1t " + jump + "\n";
            }
            if (kindOfOp == TokenType.LESS_THAN_EQUAL)
            {
            	code += "c.le.d   " + leftRegister + ",   " + rightRegister + "\n";
            	code += "bc1t " + jump + "\n";
            }
            if (kindOfOp == TokenType.GREATER_THAN_EQUAL)
            {
            	code += "c.le.d   " + rightRegister + ",   " + leftRegister + "\n";
            	code += "bc1t " + jump + "\n";
            	code += "c.eq.d   " + leftRegister + ",   " + rightRegister + "\n";
            	code += "bc1t " + jump + "\n";
            }
            if (kindOfOp == TokenType.GREATER_THAN)
            {
            	code += "c.le.d   " + rightRegister + ",   " + leftRegister + "\n";
            	code += "bc1t " + jump + "\n";
            }
            this.currentFRegister -= 4;
        }
        else
        {
        	 //Left child work
            ExpressionNode left = opNode.getLeft();
            String leftRegister = "$t" + currentTRegister++;
            code = writeExpression( left, leftRegister, jump, parentNodeType);
            //Right child work
            ExpressionNode right = opNode.getRight();
            String rightRegister = "$t" + currentTRegister++;
            code += writeExpression( right, rightRegister, jump, parentNodeType);
            
            TokenType kindOfOp = opNode.getOperation();
            //This is for when its a plus operation
            if( kindOfOp == TokenType.PLUS)
            {
                code += "add    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
          //This is for when its a minus operation
            if( kindOfOp == TokenType.MINUS)
            {
                code += "sub    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
          //This is for when its a multiply operation
            if( kindOfOp == TokenType.MULTIPLY)
            {
                code += "mult   " + leftRegister + ",   " + rightRegister + "\n";
                code += "mflo   " + resultRegister + "\n";
            }
          //This is for when its a divide operation**NOTE THIS WILL RETURN QUOTIENT FOR NOW need work to return remainder**
            if( kindOfOp == TokenType.DIVIDE)
            {
                code += "div   " + leftRegister + ",   " + rightRegister + "\n";
                code += "mflo   " + resultRegister + "\n";
            }
            //RELOP WRITTEN CODE
            if (kindOfOp == TokenType.EQUAL || kindOfOp == TokenType.EQUALCHECK)
            {
            	code += "beq   " + leftRegister + ",   " + rightRegister + ",   " + jump + "\n";
            }
            if (kindOfOp == TokenType.LESS_THAN)
            {
            	code += "blt   " + leftRegister + ",   " + rightRegister + ",   " + jump + "\n";
            }
            if (kindOfOp == TokenType.LESS_THAN_EQUAL)
            {
            	code += "ble   " + leftRegister + ",   " + rightRegister + ",   " + jump + "\n";
            }
            if (kindOfOp == TokenType.GREATER_THAN_EQUAL)
            {
            	code += "bge   " + leftRegister + ",   " + rightRegister + ",   " + jump + "\n";
            }
            if (kindOfOp == TokenType.GREATER_THAN)
            {
            	code += "bgt   " + leftRegister + ",   " + rightRegister + ",   " + jump + "\n";
            }
            this.currentTRegister -= 2;
        }
        return( code);
    }
	
    /**
     * This takes in a variable node and writes a load word for it in the machine code.
     * @param valNode The value node that will looked at.
     * @param resultRegister where the result of the value node will be put
     * @return The modified code string.
     */
    public String writeValue( ValueNode valNode, String resultRegister, DataType parentNodeType)
    {
    	if (parentNodeType == DataType.REAL)
    	{
            String value = valNode.getAttribute();
            if(valNode.getDataType() == DataType.REAL)
            {
            	//IF ONLY DO REALS
                String code = "li.d  " + resultRegister + ", " + value + " \n";
                return( code);
            }
            else
            {
                String code = "li.d  " + resultRegister + ", " + value + ".0 \n";
                return( code);
            }
    	}
    	else
    	{
            String value = valNode.getAttribute();
            String code = "addi   " + resultRegister + ",   $zero, " + value + "\n";
            return( code);
    	}
    }
    
    /**
     * This takes in a variable node and writes a load word for it in the machine code.
     * @param varNode The variable node that will looked at.
     * @param resultRegister where the result of the variable node will be put
     * @return The modified code string.
     */
    public String writeVariable(VariableNode varNode, String resultRegister, DataType parentNodeType)
    {
    	if (parentNodeType == DataType.REAL)
    	{
    		if (varNode.getDataType() == DataType.INTEGER)
    		{
    			String variable = varNode.getName();
    			String code = "l.w  " + resultRegister + ", " + variable + "\n";
    			code += "cvt.d.w  " + resultRegister + ", " + resultRegister + "\n";
        		return(code);
    		}
    		else
    		{
        		String variable = varNode.getName();
        		String code = "ldc1     " + resultRegister + ",   "  + variable + "\n";
        		return(code);
    		}
    	}
    	else
    	{
        	String variable = varNode.getName();
        	String code = "lw     " + resultRegister + ",   "  + variable + "\n";
    		return(code);
    	}
    }
    
    /** This takes in an array node and writes the code out to MIPS
     * @param arrNode The node to be written
     * @param resultRegister The resgisetr that will be written to by the node
     * @param parentNodeType The type of the parentnode of arrNOde
     * @return returns the modified code string
     */
    public String writeArray(ArrayNode arrNode, String resultRegister, DataType parentNodeType)
    {
    	if (parentNodeType == DataType.REAL)
    	{
    		if (arrNode.getDataType() == DataType.INTEGER)
    		{
    			String variable = arrNode.getName();
            	String adressregister =  "$t" + (currentTRegister++);
            	String indexregister = "$t" + (currentTRegister++);
            	String code = "ldc1     " + adressregister + ",   "  + variable + "\n";
            	code += writeExpression(arrNode.getExpression(), indexregister , null, arrNode.getDataType());
            	code += "li.d, " + indexregister + ", " + arrNode.getOffSet() + ".0\n";
            	code += "sll " + indexregister + ", " + indexregister + ", " + "2\n";
            	code += "add.d " + adressregister + ", " + adressregister + ", " + indexregister + "\n";
            	code += "lw " + resultRegister + ", 0(" + adressregister + ")\n";
    			code += "cvt.d.w  " + resultRegister + ", " + resultRegister + "\n\n";
        		return(code);
    		}
    		else
    		{
            	String variable = arrNode.getName();
            	String adressregister =  "$f" + (currentFRegister+2);
            	String indexregister = "$f" + (currentFRegister+2);
            	String code = "ldc1     " + adressregister + ",   "  + variable + "\n";
            	code += writeExpression(arrNode.getExpression(), indexregister , null, arrNode.getDataType());
            	code += "li.d " + indexregister + ", " + arrNode.getOffSet() + ".0\n";
            	code += "sll " + indexregister + ", " + indexregister + ", " + "2\n";
            	code += "add.d " + adressregister + ", " + adressregister + ", " + indexregister + "\n";
            	code += "lw " + resultRegister + ", 0(" + adressregister + ")\n";
        		code += "ldc1     " + resultRegister + ",   "  + variable + "\n\n";
        		return(code);
    		}
    	}
    	else
    	{
        	String variable = arrNode.getName();
        	String adressregister =  "$t" + currentTRegister++;
        	String indexregister = "$t" + currentTRegister++;
        	String code = "la     " + adressregister + ",   "  + variable + "\n";
        	code += writeExpression(arrNode.getExpression(), indexregister , null, arrNode.getDataType());
        	code += "addi " + indexregister + ", " + indexregister + ", " + arrNode.getOffSet() + "\n";
        	code += "sll " + indexregister + ", " + indexregister + ", " + "2\n";
        	code += "add " + adressregister + ", " + adressregister + ", " + indexregister + "\n";
        	code += "lw " + resultRegister + ", 0(" + adressregister + ")\n\n";
    		return(code);
    	}
    }
}
