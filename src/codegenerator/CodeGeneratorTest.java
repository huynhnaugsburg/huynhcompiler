package codegenerator;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.junit.Test;

import analyzer.SemanticAnalyzer;
import parser.Parser;
import syntaxtree.ProgramNode;

public class CodeGeneratorTest {


	/**
	 * This test is used to see if code can be parsed in and produce both tree before and after analysis
	 */
	@Test
	public void testCodeGenerator() throws FileNotFoundException {
		//Test on money.pas and tests inputs for all types of operations.
		String code = "./src/codegenerator/money.pas";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			String actualbefore = pn.indentedToString(0);
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			//System.out.println(pn.indentedToString(0));
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			String actualafter = pn.indentedToString(0);
			//System.out.println(pascal);
			try (PrintWriter out = new PrintWriter("money.asm")) {
			    out.println(pascal);
			}
			catch(FileNotFoundException e)
			{
				System.out.println(e.getMessage());
			}
			String exspectedbefore = 
					"Program: money\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: INTEGER : dollars\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Name: INTEGER : bitcoins\n" + 
					"|-- --- Name: INTEGER : five\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : dollars\n" + 
					"|-- --- --- Value: null : 10000\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : five\n" + 
					"|-- --- --- Value: null : 5\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 110\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- --- Operation: null : DIVIDE\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 3900\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- --- Value: null : 20\n" + 
					"|-- --- If: null\n" + 
					"|-- --- --- Operation: null : LESS_THAN\n" + 
					"|-- --- --- --- --- Value: null : 2\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- If: null\n" + 
					"|-- --- --- --- Operation: null : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 20\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 42\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 89\n" + 
					"|-- --- --- If: null\n" + 
					"|-- --- --- --- Operation: null : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 30\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 53\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 62\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- While: null\n" + 
					"|-- --- --- Operation: null : GREATER_THAN\n" + 
					"|-- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- --- Value: null : 0\n" + 
					"|-- --- --- Compound Statement\n" + 
					"|-- --- --- --- Write: null\n" + 
					"|-- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- --- Operation: null : MINUS\n" + 
					"|-- --- --- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Read: null : bitcoins\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Return: null\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Compound Statement\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 20\n" + 
					"|-- --- --- Write: null\n" + 
					"|-- --- --- --- Name: null : yen\n" + 
					"|-- --- --- Write: null\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : bitcoins\n";
			String exspectedafter = 
					"Program: money\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: INTEGER : dollars\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Name: INTEGER : bitcoins\n" + 
					"|-- --- Name: INTEGER : five\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- Value: INTEGER : 10000\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : five\n" + 
					"|-- --- --- Value: INTEGER : 5\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- Operation: INTEGER : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- --- --- Value: INTEGER : 110\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : bitcoins\n" + 
					"|-- --- --- Operation: INTEGER : DIVIDE\n" + 
					"|-- --- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- --- --- Value: INTEGER : 3900\n" + 
					"|-- --- Assignment: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- Value: INTEGER : 20\n" + 
					"|-- --- If: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : LESS_THAN\n" + 
					"|-- --- --- --- --- Value: INTEGER : 2\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- If: INTEGER\n" + 
					"|-- --- --- --- Operation: INTEGER : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 20\n" + 
					"|-- --- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 42\n" + 
					"|-- --- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 89\n" + 
					"|-- --- --- If: INTEGER\n" + 
					"|-- --- --- --- Operation: INTEGER : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 30\n" + 
					"|-- --- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 53\n" + 
					"|-- --- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 62\n" + 
					"|-- --- Write: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : yen\n" + 
					"|-- --- While: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : GREATER_THAN\n" + 
					"|-- --- --- --- --- Name: INTEGER : five\n" + 
					"|-- --- --- --- --- Value: INTEGER : 0\n" + 
					"|-- --- --- Compound Statement\n" + 
					"|-- --- --- --- Write: INTEGER\n" + 
					"|-- --- --- --- --- Name: INTEGER : five\n" + 
					"|-- --- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- --- Name: INTEGER : five\n" + 
					"|-- --- --- --- --- Operation: INTEGER : MINUS\n" + 
					"|-- --- --- --- --- --- --- Name: INTEGER : five\n" + 
					"|-- --- --- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Read: INTEGER : bitcoins\n" + 
					"|-- --- Write: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : bitcoins\n" + 
					"|-- --- Write: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Return: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Compound Statement\n" + 
					"|-- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- Assignment: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- --- --- Operation: INTEGER : MULTIPLY\n" + 
					"|-- --- --- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 20\n" + 
					"|-- --- --- Write: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : yen\n" + 
					"|-- --- --- Write: INTEGER\n" + 
					"|-- --- --- --- Name: INTEGER : dollars\n" + 
					"|-- --- Write: INTEGER\n" + 
					"|-- --- --- Name: INTEGER : bitcoins\n";
			assertEquals(actualbefore,exspectedbefore);
			assertEquals(actualafter,exspectedafter);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize program!");
		}
	}
	
	@Test
	public void testCodeGeneratorFloats() throws FileNotFoundException {
		//Test on money.pas and tests inputs for all types of operations.
		String code = "./src/codegenerator/moneyfloats.pas";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			String actualbefore = pn.indentedToString(0);
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			//System.out.println(pn.indentedToString(0));
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			String actualafter = pn.indentedToString(0);
			//System.out.println(pascal);
			try (PrintWriter out = new PrintWriter("moneyfloat.asm")) {
			    out.println(pascal);
			}
			catch(FileNotFoundException e)
			{
				System.out.println(e.getMessage());
			}
			String exspectedbefore = 
					"Program: money\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: REAL : dollars\n" + 
					"|-- --- Name: REAL : yen\n" + 
					"|-- --- Name: REAL : bitcoins\n" + 
					"|-- --- Name: REAL : five\n" + 
					"|-- --- Name: REAL : sevenpointtwo\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : sevenpointtwo\n" + 
					"|-- --- --- Value: null : 7.2\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : dollars\n" + 
					"|-- --- --- Value: null : 10000\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : five\n" + 
					"|-- --- --- Value: null : 5\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 110\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- --- Operation: null : DIVIDE\n" + 
					"|-- --- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- --- Value: null : 3900\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- --- Value: null : 20\n" + 
					"|-- --- If: null\n" + 
					"|-- --- --- Operation: null : LESS_THAN\n" + 
					"|-- --- --- --- --- Value: null : 2\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- If: null\n" + 
					"|-- --- --- --- Operation: null : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 20\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 42\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 89\n" + 
					"|-- --- --- If: null\n" + 
					"|-- --- --- --- Operation: null : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 30\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 53\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- Value: null : 62\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : yen\n" + 
					"|-- --- While: null\n" + 
					"|-- --- --- Operation: null : GREATER_THAN\n" + 
					"|-- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- --- Value: null : 0\n" + 
					"|-- --- --- Compound Statement\n" + 
					"|-- --- --- --- Write: null\n" + 
					"|-- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- Assignment: null\n" + 
					"|-- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- --- Operation: null : MINUS\n" + 
					"|-- --- --- --- --- --- --- Name: null : five\n" + 
					"|-- --- --- --- --- --- --- Value: null : 0.1\n" + 
					"|-- --- Read: null : bitcoins\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Return: null\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Compound Statement\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- Assignment: null\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- --- --- Operation: null : MULTIPLY\n" + 
					"|-- --- --- --- --- --- Name: null : yen\n" + 
					"|-- --- --- --- --- --- Value: null : 20\n" + 
					"|-- --- --- Write: null\n" + 
					"|-- --- --- --- Name: null : yen\n" + 
					"|-- --- --- Write: null\n" + 
					"|-- --- --- --- Name: null : dollars\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : bitcoins\n" + 
					"|-- --- Write: null\n" + 
					"|-- --- --- Name: null : sevenpointtwo\n";
			String exspectedafter = 
					"Program: money\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: REAL : dollars\n" + 
					"|-- --- Name: REAL : yen\n" + 
					"|-- --- Name: REAL : bitcoins\n" + 
					"|-- --- Name: REAL : five\n" + 
					"|-- --- Name: REAL : sevenpointtwo\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : sevenpointtwo\n" + 
					"|-- --- --- Value: REAL : 7.2\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- Value: INTEGER : 10000\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : five\n" + 
					"|-- --- --- Value: INTEGER : 5\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : yen\n" + 
					"|-- --- --- Operation: REAL : MULTIPLY\n" + 
					"|-- --- --- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- --- --- Value: INTEGER : 110\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : bitcoins\n" + 
					"|-- --- --- Operation: REAL : DIVIDE\n" + 
					"|-- --- --- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- --- --- Value: INTEGER : 3900\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : yen\n" + 
					"|-- --- --- Value: INTEGER : 20\n" + 
					"|-- --- If: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : LESS_THAN\n" + 
					"|-- --- --- --- --- Value: INTEGER : 2\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- If: REAL\n" + 
					"|-- --- --- --- Operation: REAL : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 20\n" + 
					"|-- --- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 42\n" + 
					"|-- --- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 89\n" + 
					"|-- --- --- If: REAL\n" + 
					"|-- --- --- --- Operation: REAL : EQUAL\n" + 
					"|-- --- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 30\n" + 
					"|-- --- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 53\n" + 
					"|-- --- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- Value: INTEGER : 62\n" + 
					"|-- --- Write: REAL\n" + 
					"|-- --- --- Name: REAL : yen\n" + 
					"|-- --- While: REAL\n" + 
					"|-- --- --- Operation: REAL : GREATER_THAN\n" + 
					"|-- --- --- --- --- Name: REAL : five\n" + 
					"|-- --- --- --- --- Value: INTEGER : 0\n" + 
					"|-- --- --- Compound Statement\n" + 
					"|-- --- --- --- Write: REAL\n" + 
					"|-- --- --- --- --- Name: REAL : five\n" + 
					"|-- --- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- --- Name: REAL : five\n" + 
					"|-- --- --- --- --- Operation: REAL : MINUS\n" + 
					"|-- --- --- --- --- --- --- Name: REAL : five\n" + 
					"|-- --- --- --- --- --- --- Value: REAL : 0.1\n" + 
					"|-- --- Read: REAL : bitcoins\n" + 
					"|-- --- Write: REAL\n" + 
					"|-- --- --- Name: REAL : bitcoins\n" + 
					"|-- --- Write: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Return: INTEGER\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Compound Statement\n" + 
					"|-- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- Assignment: REAL\n" + 
					"|-- --- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- --- Operation: REAL : MULTIPLY\n" + 
					"|-- --- --- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 20\n" + 
					"|-- --- --- Write: REAL\n" + 
					"|-- --- --- --- Name: REAL : yen\n" + 
					"|-- --- --- Write: REAL\n" + 
					"|-- --- --- --- Name: REAL : dollars\n" + 
					"|-- --- Write: REAL\n" + 
					"|-- --- --- Name: REAL : bitcoins\n" + 
					"|-- --- Write: REAL\n" + 
					"|-- --- --- Name: REAL : sevenpointtwo\n";
			assertEquals(actualbefore,exspectedbefore);
			assertEquals(actualafter,exspectedafter);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize program!");
		}
	}
	
	@Test
	public void testCodeGeneratorArrays() throws FileNotFoundException {
		//Test on money.pas and tests inputs for all types of operations.
		String code = "./src/codegenerator/moneyarrays.pas";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			String actualbefore = pn.indentedToString(0);
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			//System.out.println(pn.indentedToString(0));
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			String actualafter = pn.indentedToString(0);
			//System.out.println(pascal);
			try (PrintWriter out = new PrintWriter("moneyarrays.asm")) {
			    out.println(pascal);
			}
			catch(FileNotFoundException e)
			{
				System.out.println(e.getMessage());
			}
			String exspectedbefore = 
					"Program: money\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: REAL : dollars\n" + 
					"|-- --- Size: 5\n" + 
					"|-- --- Offset: -1\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- Value: null : 1\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Size: 5\n" + 
					"|-- --- Offset: -1\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: null\n" + 
					"|-- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- Size: 5\n" + 
					"|-- --- --- Offset: -1\n" + 
					"|-- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- --- Value: null : 1\n" + 
					"|-- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- --- --- Size: 5\n" + 
					"|-- --- --- --- --- Offset: -1\n" + 
					"|-- --- --- --- --- --- Operation: null : PLUS\n" + 
					"|-- --- --- --- --- --- --- --- Value: null : 5\n" + 
					"|-- --- --- --- --- --- --- --- Value: null : 1\n";
			String exspectedafter = 
					"Program: money\n" + 
					"|-- Declarations\n" + 
					"|-- --- Name: REAL : dollars\n" + 
					"|-- --- Size: 5\n" + 
					"|-- --- Offset: -1\n" + 
					"|-- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- Name: INTEGER : yen\n" + 
					"|-- --- Size: 5\n" + 
					"|-- --- Offset: -1\n" + 
					"|-- SubProgramDeclarations\n" + 
					"|-- Compound Statement\n" + 
					"|-- --- Assignment: REAL\n" + 
					"|-- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- Size: 5\n" + 
					"|-- --- --- Offset: -1\n" + 
					"|-- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- --- Value: INTEGER : 1\n" + 
					"|-- --- --- Operation: REAL : PLUS\n" + 
					"|-- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- Name: REAL : dollars\n" + 
					"|-- --- --- --- --- Size: 5\n" + 
					"|-- --- --- --- --- Offset: -1\n" + 
					"|-- --- --- --- --- --- Operation: INTEGER : PLUS\n" + 
					"|-- --- --- --- --- --- --- --- Value: INTEGER : 5\n" + 
					"|-- --- --- --- --- --- --- --- Value: INTEGER : 1\n";
			assertEquals(actualbefore,exspectedbefore);
			assertEquals(actualafter,exspectedafter);
		}
		catch(RuntimeException e)
		{
			System.out.println(e.getMessage());
			fail("Failed to recognize program!");
		}
	}
	
	@Test
	public void testWriteAssignments() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"var variable2: real;\n" +
				"begin\n" + 
				"variable := 2 + 2;\n" +
				"variable2 := 2.0 + 2.0\n" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"variable2:   .double   0.0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"addi   $t0,   $zero, 2\n" + 
					"addi   $t1,   $zero, 2\n" + 
					"add    $s0,   $t0,   $t1\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"li.d  $f4, 2.0 \n" + 
					"li.d  $f6, 2.0 \n" + 
					"add.d    $f0,   $f4,   $f6\n" + 
					"sdc1     $f0,   variable2\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteCompoundStatement() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"var variable2: real;\n" +
				"begin\n" + 
				"  begin \n" + 
				"	variable:= 5 + 5;\n" + 
				"	variable2 := variable * 20;\n" + 
				"	write(variable)"
				+ "	end\n" +  
				"  end" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"variable2:   .double   0.0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"addi   $t0,   $zero, 5\n" + 
					"addi   $t1,   $zero, 5\n" + 
					"add    $s0,   $t0,   $t1\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"l.w  $f4, variable\n" + 
					"cvt.d.w  $f4, $f4\n" + 
					"li.d  $f6, 20.0 \n" + 
					"mul.d    $f0,   $f4,   $f6\n" + 
					"sdc1     $f0,   variable2\n" + 
					"\n" + 
					"lw     $s0,   variable\n" + 
					"li $v0, 1\n" + 
					"add $a0, $s0, $zero\n" + 
					"syscall\n" + 
					"\n" + 
					"addi $a0, $0, 0xA\n" + 
					"addi $v0, $0, 0xB\n" + 
					"syscall\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n" + 
					"";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteIfStatement() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"begin\n" + 
				"if 2 < 5 then variable := 10 + 10 else variable := 10 - 10" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"If1:\n" + 
					"addi   $t0,   $zero, 2\n" + 
					"addi   $t1,   $zero, 5\n" + 
					"blt   $t0,   $t1,   Then1\n" + 
					"\n" + 
					"Else1:\n" + 
					"addi   $t0,   $zero, 10\n" + 
					"addi   $t1,   $zero, 10\n" + 
					"sub    $s0,   $t0,   $t1\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"j	Endif1\n" + 
					"\n" + 
					"Then1:\n" + 
					"addi   $t0,   $zero, 10\n" + 
					"addi   $t1,   $zero, 10\n" + 
					"add    $s0,   $t0,   $t1\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"Endif1:\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteWhileDoStatement() {
		String code = "program money;\n" + 
				"var five: integer;\n" + 
				"begin\n" + 
				"  while five > 0 \n" + 
				"  do\n" + 
				"	begin\n" + 
				"		write(five);\n" + 
				"		five := five - 1\n" + 
				"	end\n" + 
				"end\n" + 
				".\n" + 
				"";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n"
					+ "five:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"While1:\n" + 
					"lw     $t0,   five\n" + 
					"addi   $t1,   $zero, 0\n" + 
					"bgt   $t0,   $t1,   Do1\n" + 
					"\n" + 
					"j	EndWhileDo1\n" + 
					"\n" + 
					"Do1:\n" + 
					"lw     $s0,   five\n" + 
					"li $v0, 1\n" + 
					"add $a0, $s0, $zero\n" + 
					"syscall\n" + 
					"\n" + 
					"addi $a0, $0, 0xA\n" + 
					"addi $v0, $0, 0xB\n" + 
					"syscall\n" + 
					"\n" + 
					"lw     $t0,   five\n" + 
					"addi   $t1,   $zero, 1\n" + 
					"sub    $s0,   $t0,   $t1\n" + 
					"sw     $s0,   five\n" + 
					"\n" + 
					"j	While1\n" + 
					"\n" + 
					"EndWhileDo1:\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteReadStatement() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"begin\n" + 
				"read(variable)" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"li $v0, 5\n" + 
					"syscall\n" + 
					"sw $v0, variable\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteWriteStatement() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"begin\n" + 
				"write(2 + 2)" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"addi   $t0,   $zero, 2\n" + 
					"addi   $t1,   $zero, 2\n" + 
					"add    $s0,   $t0,   $t1\n" + 
					"li $v0, 1\n" + 
					"add $a0, $s0, $zero\n" + 
					"syscall\n" + 
					"\n" + 
					"addi $a0, $0, 0xA\n" + 
					"addi $v0, $0, 0xB\n" + 
					"syscall\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n" + 
					"";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteReturnStatement() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"begin\n" + 
				"return 2 + 2" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"addi   $t0,   $zero, 2\n" + 
					"addi   $t1,   $zero, 2\n" + 
					"add    $v0,   $t0,   $t1\n" + 
					"\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteOperation() {
		String code = "program money;\n" + 
				"var variable: integer;\n" + 
				"begin\n" + 
				"variable := 2 + 2 * 5 - 6 / 12" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"addi   $t1,   $zero, 2\n" + 
					"addi   $t3,   $zero, 2\n" + 
					"addi   $t4,   $zero, 5\n" + 
					"mult   $t3,   $t4\n" + 
					"mflo   $t2\n" + 
					"add    $t0,   $t1,   $t2\n" + 
					"addi   $t2,   $zero, 6\n" + 
					"addi   $t3,   $zero, 12\n" + 
					"div   $t2,   $t3\n" + 
					"mflo   $t1\n" + 
					"sub    $s0,   $t0,   $t1\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteValue() {
		String code = "program money;\n" + 
				"var variable, ten: integer;\n" + 
				"begin\n" + 
				"variable := 10\n" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"ten:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"addi   $s0,   $zero, 10\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

	@Test
	public void testWriteVariable() {
		String code = "program money;\n" + 
				"var variable, ten: integer;\n" + 
				"begin\n" + 
				"variable := ten\n" +
				"end\n" + 
				".";
		Parser recog = new Parser(code);
		try
		{
			ProgramNode pn = recog.program();
			SemanticAnalyzer analyzer = new SemanticAnalyzer(pn, recog.getTable());
			analyzer.analyze();
			String exspected = ".data\n" + 
					"variable:   .word   0\n" + 
					"ten:   .word   0\n" + 
					"\n" + 
					"\n" + 
					".text\n" + 
					"main:\n" + 
					"lw     $s0,   ten\n" + 
					"sw     $s0,   variable\n" + 
					"\n" + 
					"li  $v0,   10\n" + 
					"syscall\n";
			CodeGenerator generator =  new CodeGenerator();
			String pascal = generator.writeCodeForProgram(pn);
			assertEquals(pascal,exspected);
		}
		catch(RuntimeException e)
		{
				System.out.println(e.getMessage());
				fail("Failed to recognize program!");
		}
	}

}
