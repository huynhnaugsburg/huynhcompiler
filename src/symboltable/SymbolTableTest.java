/**
 * Symboltable.java
 * Compilers 2
 * February 24, 2019
 * This tests the SymbolTable.java code and makes sure the methods are working as intended.
 * @author Nghia Huynh
 * @version 2
 */
package symboltable;

import static org.junit.Assert.*;

import org.junit.Test;

import analyzer.DataType;

public class SymbolTableTest {

	/**
	 * Tests to see if a new table can be made and a new symbol added to it properly
	 */
	@Test
	public void testAddSymbol() {
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("Apple", KindType.PROGRAM, DataType.INTEGER);
		String exspectedname = "Apple";
		String exspectedkind = KindType.PROGRAM.toString();
		String exspectedtype = DataType.INTEGER.toString();
		assertEquals(exspectedname,testtable.getSymbolStuff("Apple","name"));
		assertEquals(exspectedkind,testtable.getSymbolStuff("Apple","kind"));
		assertEquals(exspectedtype,testtable.getSymbolStuff("Apple","type"));
	}

	/**
	 * Tests to see if the code can check if something is of kind program.
	 */
	@Test
	public void testIsProgram() {
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("Apple", KindType.PROGRAM, DataType.INTEGER);
		boolean exspectedvalue1 = true;
		boolean exspectedvalue2 = false;
		assertEquals(exspectedvalue1, testtable.isProgram("Apple"));
		assertEquals(exspectedvalue2, testtable.isVariable("Apple"));
		assertEquals(exspectedvalue2, testtable.isFunction("Apple"));
		assertEquals(exspectedvalue2, testtable.isProcedure("Apple"));
	}

	/**
	 * Tests to see if the code can check if something is of kind variable.
	 */
	@Test
	public void testIsVariable() {
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("Apple", KindType.VARIABLE, DataType.INTEGER);
		boolean exspectedvalue1 = true;
		boolean exspectedvalue2 = false;
		assertEquals(exspectedvalue1, testtable.isVariable("Apple"));
		assertEquals(exspectedvalue2, testtable.isFunction("Apple"));
		assertEquals(exspectedvalue2, testtable.isProcedure("Apple"));
		assertEquals(exspectedvalue2, testtable.isProgram("Apple"));
	}

	/**
	 * Tests to see if the code can check if something is of kind function.
	 */
	@Test
	public void testIsFunction() {
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("Apple", KindType.FUNCTION, DataType.INTEGER);
		boolean exspectedvalue1 = true;
		boolean exspectedvalue2 = false;
		assertEquals(exspectedvalue1, testtable.isFunction("Apple"));
		assertEquals(exspectedvalue2, testtable.isProcedure("Apple"));
		assertEquals(exspectedvalue2, testtable.isProgram("Apple"));
		assertEquals(exspectedvalue2, testtable.isVariable("Apple"));
	}

	/**
	 * Tests to see if the code can check if something is of kind procedure.
	 */
	@Test
	public void testIsProcedure() {
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("Apple", KindType.PROCEDURE, DataType.INTEGER);
		boolean exspectedvalue1 = true;
		boolean exspectedvalue2 = false;
		assertEquals(exspectedvalue1, testtable.isProcedure("Apple"));
		assertEquals(exspectedvalue2, testtable.isProgram("Apple"));
		assertEquals(exspectedvalue2, testtable.isVariable("Apple"));
		assertEquals(exspectedvalue2, testtable.isFunction("Apple"));
	}
	
	/**
	 * Tests to see if the to string method is outputting the correct format for the table.
	 */
	@Test
	public void testToString()
	{
		SymbolTable testtable = new SymbolTable();
		testtable.addSymbol("Apple", KindType.PROCEDURE, DataType.INTEGER);
		testtable.addSymbol("Oarnge", KindType.VARIABLE, DataType.INTEGER);
		testtable.addSymbol("Banana", KindType.FUNCTION, DataType.INTEGER);
		testtable.addSymbol("Watermelon", KindType.PROGRAM, DataType.INTEGER);
		String exspected = String.format(" %-20s", "Symbol") + 
							String.format(" %-20s", "Kind") + 
							String.format(" %-20s", "Type") + "\n";
		exspected = exspected + "----------------------------------------------------------------\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","Apple", KindType.PROCEDURE, DataType.INTEGER) + "\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","Watermelon", KindType.PROGRAM, DataType.INTEGER) + "\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","Oarnge", KindType.VARIABLE, DataType.INTEGER) + "\n";
		exspected = exspected + String.format("|%-20s|%-20s|%-20s|","Banana", KindType.FUNCTION, DataType.INTEGER) + "\n";
		assertEquals(exspected, testtable.toString());
		
	}

}
