/**
 * Symboltable.java
 * Compilers 2
 * February 24, 2019
 * Holds a symbol table and uses a hashmap to store IDs that are used inside of the mini pascal compiler.
 * @author Nghia Huynh
 * @version 2
 */
package symboltable;

import java.util.HashMap;

import analyzer.DataType;

public class SymbolTable {
	/**
	 * This is the hashmap used to hold the symbols for the minipascal language
	 */
	private HashMap<String, Symbol>  table = new HashMap<String, Symbol>();

	/**
	 * This puts a new symbol into the symbol table hashmap.
	 * @param newname	name for a new symbol
	 * @param newkind	kind for a new symbol
	 * @param newtype	type for a new symbol
	 */
	public void addSymbol(String newname, KindType newkind, DataType newtype)
	{
		table.put(newname, new Symbol(newname, newkind, newtype));
	}

	/**
	 * NOTE: THIS METHOD IS NOT  USED FOR THE CLASS AND IS USED TO HELP TEST THE addSymbol function.
	 * just returns parts of the symbol object of the internal table.
	 * @param key the key for the specific symbol.
	 * @param item this tells the method what variable of Symbol it shoudl return.
	 * @return the symbol object.
	 */
	public String getSymbolStuff(String key, String item)
	{
		String testoutput = null;
		if(item == "name")
		{
			 testoutput = table.get(key).name.toString();
		}
		if(item == "kind")
		{
			testoutput = table.get(key).kind.toString();
		}
		if(item == "type")
		{
			testoutput = table.get(key).type.toString();
		}
		return testoutput;
	}
	
/////////////////////////////////////////////////////////////////////////////////
//THE BELOW CODE IS USED TO CHECK FOR TYPES BASED ON A GIVEN ID NAME.
/////////////////////////////////////////////////////////////////////////////////
	/**
	 * Takes in a symbol name and checks if it is program type and returns true if it is else false.
	 * @param newname name of symbol
	 * @return boolean true or false
	 */
	public boolean isProgram(String newname)
	{
		Symbol temp = table.get(newname);
		if(temp != null && table.get(newname).kind == KindType.PROGRAM)
		{
			return true;
		}
		return false;
	}
	/**
	 * Takes in a symbol name and checks if it is variable type and returns true if it is else false.
	 * @param newname name of symbol
	 * @return boolean true or false
	 */
	public boolean isVariable(String newname)
	{
		Symbol temp = table.get(newname);
		if(temp != null && table.get(newname).kind == KindType.VARIABLE)
		{
			return true;
		}
		return false;
	}
	/**
	 * Takes in a symbol name and checks if it is function type and returns true if it is else false.
	 * @param newname name of symbol
	 * @return boolean true or false
	 */
	public boolean isFunction(String newname)
	{
		Symbol temp = table.get(newname);
		if(temp != null && table.get(newname).kind == KindType.FUNCTION)
		{
			return true;
		}
		return false;
	}
	/**
	 * Takes in a symbol name and checks if it is procedure type and returns true if it is else false.
	 * @param newname name of symbol
	 * @return boolean true or false
	 */
	public boolean isProcedure(String newname)
	{
		Symbol temp = table.get(newname);
		if(temp != null && table.get(newname).kind == KindType.PROCEDURE)
		{
			return true;
		}
		return false;
	}
	
	public DataType isType(String newname)
	{
		Symbol temp = table.get(newname);
		if(temp == null)
		{
			return null;
		}
		return temp.type;
	}
	
	/**
	 * This code creates a chart using the toString method and shows the different symbols and their kind.
	 */
	public String toString()
	{
		String output = String.format(" %-20s", "Symbol") + 
						String.format(" %-20s", "Kind") + 
						String.format(" %-20s", "Type") + "\n";
		output = output + "----------------------------------------------------------------\n";
		for (HashMap.Entry<String, Symbol> key : table.entrySet())
		{
			String str1 = String.format("|%-20s",key.getValue().name);
			String str2 = String.format("|%-20s",key.getValue().kind);
			String str3 = String.format("|%-20s|",key.getValue().type);
			output = output + str1 + str2 + str3 + "\n";
		}
		
		return output;
	}
	
/////////////////////////////////////////////////////////////////////////////////
//THE BELOW CODE IS USED TO HOLD A PRIVATE SYMBOL OBJECT
/////////////////////////////////////////////////////////////////////////////////
	private class Symbol {
		private String name; //Holds the name of the symbol
		private KindType kind;//Holds the kind of the symbol
		private DataType type;//holds the type of the symbol NOTE: not used yet.
		
		/**
		 * This puts a new symbol into the symbol table hashmap.
		 * @param newname	name for a new symbol
		 * @param newkind	kind for a new symbol
		 * @param newtype	type for a new symbol
		 */
		private Symbol(String newname, KindType newkind, DataType newtype)
		{
			this.name = newname;
			this.kind = newkind;
			this.type = newtype;
		}
	}
}

