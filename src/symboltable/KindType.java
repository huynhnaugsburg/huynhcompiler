/**
 *KindType.java
 * Compilers 2
 * February 24, 2019
 * This holds the enum for all of the different kind types found in our simple pascal.
 * @author Nghia Huynh
 * @version 2
 */

package symboltable;

public enum KindType {
	PROGRAM, VARIABLE, PROCEDURE, FUNCTION;
}
