/**
 * KindTypeTest.java
 * Compilers 2
 * February 24, 2019
 * This tests to see if the enums are functioning correctly.
 * @author Nghia Huynh
 * @version 1
 */

package symboltable;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *This test it to make sure all of the enums are correct and link to the correct kind type.
 *
 */
public class KindTypeTest {

	@Test
	public void test() {
		assertEquals(KindType.valueOf("PROGRAM"), KindType.PROGRAM);
		assertEquals(KindType.valueOf("VARIABLE"), KindType.VARIABLE);
		assertEquals(KindType.valueOf("PROCEDURE"), KindType.PROCEDURE);
		assertEquals(KindType.valueOf("FUNCTION"), KindType.FUNCTION);
	}

}
